init -10 python:
    if persistent.save_page is None:
        persistent.save_page = 1

    if persistent.count_granhigi is None:
        persistent.count_granhigi = 0

    if persistent.alicek is None:
        persistent.alicek = 0

    # сколько раз призывал духов
    if persistent.count_wind is None:
        persistent.count_wind = 0

    if persistent.count_earth is None:
        persistent.count_earth = 0

    if persistent.count_aqua is None:
        persistent.count_aqua = 0

    if persistent.count_fire is None:
        persistent.count_fire = 0

    # сколько раз враг призывал духов
    if persistent.count_enemy_wind is None:
        persistent.count_enemy_wind = 0

    if persistent.count_enemy_earth is None:
        persistent.count_enemy_earth = 0

    if persistent.count_enemy_aqua is None:
        persistent.count_enemy_aqua = 0

    if persistent.count_enemy_fire is None:
        persistent.count_enemy_fire = 0

    if persistent.counterk is None:
        persistent.counterk = 0

    if persistent.tamek is None:
        persistent.tamek = 0

    if persistent.dopple_wet is None:
        persistent.dopple_wet = 0

    if persistent.hsean_shitenno5 is None:
        persistent.hsean_shitenno5 = 0

    if persistent.micaela_batou is None:
        persistent.micaela_batou = 0

    if persistent.lonely is None:
        persistent.lonely = 0

    if persistent.ruka_book is None:
        persistent.ruka_book = 0

    if persistent.count_sara is None:
        persistent.count_sara = 0

    if persistent.count_mylv0 is None:
        persistent.count_mylv0 = 0

    if persistent.count_kadora is None:
        persistent.count_kadora = 0

    # доказательство героя
    if persistent.yuusyaakasi is None:
        persistent.yuusyaakasi = 0

    # уставшая Сильфа
    if persistent.g_sylph is None:
        persistent.g_sylph = 0

    # уснувшая Гнома
    if persistent.z_gnome is None:
        persistent.z_gnome = 0

    if persistent.count_alice1 is None:
        persistent.count_alice1 = 0

    # победа с одного хода
    if persistent.count_onekill is None:
        persistent.count_onekill = 0

    # победа будучи в замешательстве
    if persistent.count_vickonran is None:
        persistent.count_vickonran = 0

    # счетчик капитуляций
    if persistent.count_kousan is None:
        persistent.count_kousan = 0

    # показывать или нет эро сцены
    if persistent.hsean_cut is None:
        persistent.hsean_cut = 0

    # показывать или нет сцены пожираний
    if persistent.hosyoku_cut is None:
        persistent.hosyoku_cut = 0

    # если алиса победила монстра
    if persistent.count_alice2 is None:
        persistent.count_alice2 = 0

    # счетчик всех побед
    if persistent.count_ko is None:
        persistent.count_ko = 0

    # счетчик поражений от конкретного монстра
    if persistent.count_koenemy is None:
        persistent.count_koenemy = 0

    # имя монстра которому больше всего проиграл
    if persistent.count_mostname is None:
        persistent.count_mostname = 0

    # счетчик высосанной энергии
    if persistent.count_drainhp is None:
        persistent.count_drainhp = 0

    # счетчик высосанных уровней
    if persistent.count_drainlv is None:
        persistent.count_drainlv = 0

    # колличество поражений от монстра которому больше всего проиграл
    if persistent.count_most is None:
        persistent.count_most = 0

    # проверка на полное прохождение игры
    if persistent.game_clear is None:
        persistent.game_clear = 0

    # счетчик-проверка открытия всех навыков монстров в первой части
    if persistent.count_oskill1 is None:
        persistent.count_oskill1 = 0

    # счетчик-проверка открытия всех навыков монстров во второй части
    if persistent.count_oskill2 is None:
        persistent.count_oskill2 = 0

    # счетчик-проверка открытия всех навыков монстров в третьей части
    if persistent.count_oskill3 is None:
        persistent.count_oskill3 = 0

    # колличество запрошенных навыков
    if persistent.count_onedari is None:
        persistent.count_onedari = 0

    if persistent.count_end is None:
        persistent.count_end = 0

    # колличество советов спрошенных в первой части
    if persistent.count_hanseikai is None:
        persistent.count_hanseikai = 0

    # колличество советов спрошенных во второй части
    if persistent.count_hanseikai2 is None:
        persistent.count_hanseikai2 = 0

    # проверка на полное открытие Монстропедии первой части
    if persistent.count_zukan1 is None:
        persistent.count_zukan1 = 0

    # проверка на полное открытие Монстропедии второй части
    if persistent.count_zukan2 is None:
        persistent.count_zukan2 = 0

    # проверка на полное открытие Монстропедии третьей части
    if persistent.count_zukan3 is None:
        persistent.count_zukan3 = 0

    # проверка на победу на адском уровне сложности
    if persistent.count_hellvic is None:
        persistent.count_hellvic = 0

    # различные счетчики для эякуляций и т.п.
    if persistent.count_nakadashi is None:
        persistent.count_nakadashi = 0

    if persistent.count_sakuseikikan is None:
        persistent.count_sakuseikikan = 0

    if persistent.count_bouhatu is None:
        persistent.count_bouhatu = 0

    if persistent.count_sumata is None:
        persistent.count_sumata = 0

    if persistent.count_fera is None:
        persistent.count_fera = 0

    if persistent.count_tekoki is None:
        persistent.count_tekoki = 0

    if persistent.count_ashikoki is None:
        persistent.count_ashikoki = 0

    if persistent.count_paizuri is None:
        persistent.count_paizuri = 0

    if persistent.count_analsex is None:
        persistent.count_analsex = 0

    if persistent.count_analseme is None:
        persistent.count_analseme = 0

    if persistent.count_syokusyu is None:
        persistent.count_syokusyu = 0

    if persistent.count_slime is None:
        persistent.count_slime = 0

    if persistent.count_plant is None:
        persistent.count_plant = 0

    if persistent.count_wing is None:
        persistent.count_wing = 0

    if persistent.count_buturi is None:
        persistent.count_buturi = 0

    if persistent.count_nenmaku is None:
        persistent.count_nenmaku = 0

    if persistent.count_role is None:
        persistent.count_role = 0

    if persistent.count_ganmenkizyou is None:
        persistent.count_ganmenkizyou = 0

    if persistent.count_kiss is None:
        persistent.count_kiss = 0

    if persistent.count_sipppo is None:
        persistent.count_sipppo = 0

    if persistent.count_ito is None:
        persistent.count_ito = 0

    if persistent.count_onani is None:
        persistent.count_onani = 0

    if persistent.count_kami is None:
        persistent.count_kami = 0

    if persistent.count_tikubi is None:
        persistent.count_tikubi = 0

    if persistent.count_zenshin is None:
        persistent.count_zenshin = 0

    if persistent.count_kusuguri is None:
        persistent.count_kusuguri = 0

    if persistent.count_kaze is None:
        persistent.count_kaze = 0

    if persistent.count_ihuku is None:
        persistent.count_ihuku = 0

    if persistent.count_zensin is None:
        persistent.count_zensin = 0

    if persistent.count_houyou is None:
        persistent.count_houyou = 0

    if persistent.count_tissoku is None:
        persistent.count_tissoku = 0

    if persistent.count_yuuwaku is None:
        persistent.count_yuuwaku = 0

    if persistent.count_sonota is None:
        persistent.count_sonota = 0

    if persistent.count_marunomi is None:
        persistent.count_marunomi = 0

    if persistent.count_sirikoki is None:
        persistent.count_sirikoki = 0

    if persistent.count_kousoku is None:
        persistent.count_kousoku = 0

    if persistent.count_koukotu is None:
        persistent.count_koukotu = 0

    if persistent.count_mahi is None:
        persistent.count_mahi = 0

    if persistent.count_yuwaku is None:
        persistent.count_yuwaku = 0

    if persistent.count_kuppuku is None:
        persistent.count_kuppuku = 0

    if persistent.count_sekika is None:
        persistent.count_sekika = 0

    if persistent.count_konran is None:
        persistent.count_konran = 0

    if persistent.count_b001 is None:
        persistent.count_b001 = 0

    if persistent.count_b002 is None:
        persistent.count_b002 = 0

    if persistent.count_b003 is None:
        persistent.count_b003 = 0

    if persistent.count_b004 is None:
        persistent.count_b004 = 0

    if persistent.count_b005 is None:
        persistent.count_b005 = 0

    if persistent.count_b006 is None:
        persistent.count_b006 = 0

    if persistent.count_b007 is None:
        persistent.count_b007 = 0

    if persistent.count_b008 is None:
        persistent.count_b008 = 0

    if persistent.count_b009 is None:
        persistent.count_b009 = 0

    if persistent.count_b010 is None:
        persistent.count_b010 = 0

    if persistent.count_b011 is None:
        persistent.count_b011 = 0

    if persistent.count_b012 is None:
        persistent.count_b012 = 0

    if persistent.count_b013 is None:
        persistent.count_b013 = 0

    if persistent.count_b014 is None:
        persistent.count_b014 = 0

    if persistent.count_b015 is None:
        persistent.count_b015 = 0

    if persistent.count_b016 is None:
        persistent.count_b016 = 0

    if persistent.count_b017 is None:
        persistent.count_b017 = 0

    # элементы управления изображением в монстропедии
    if persistent.bk1 is None:
        persistent.bk1 = 0

    if persistent.bk2 is None:
        persistent.bk2 = 0

    if persistent.bk3 is None:
        persistent.bk3 = 0

    if persistent.bk4 is None:
        persistent.bk4 = 0

    if persistent.bk5 is None:
        persistent.bk5 = 0

    if persistent.bk6 is None:
        persistent.bk6 = 0

    if persistent.bk7 is None:
        persistent.bk7 = 0

    if persistent.bk8 is None:
        persistent.bk8 = 0

    if persistent.pose is None:
        persistent.pose = 1

    if persistent.face is None:
        persistent.face = 1

    if persistent.battle is None:
        persistent.battle = 0

    if persistent.echi is None:
        persistent.echi = ""

    # поражения от конкретного монстра
    if persistent.defeat is None:
        persistent.defeat = {
            # Часть 1
            "slime": 0,
            "alice1": 0,
            "slug": 0,
            "mdg": 0,
            "granberia1": 0,
            "mimizu": 0,
            "gob": 0,
            "pramia": 0,
            "vgirl": 0,
            "dragonp": 0,
            "mitubati": 0,
            "hapy_a": 0,
            "hapy_bc": 0,
            "queenharpy": 0,
            "delf_a": 0,
            "delf_b": 0,
            "hiru": 0,
            "rahure": 0,
            "ropa": 0,
            "youko": 0,
            "meda": 0,
            "kumo": 0,
            "mimic": 0,
            "nanabi": 0,
            "tamamo1": 0,
            "alma_elma1": 0,
            "namako": 0,
            "kai": 0,
            "lamia": 0,
            "granberia2": 0,
            "page17": 0,
            "page257": 0,
            "page65537": 0,
            "kani": 0,
            "kurage": 0,
            "iso": 0,
            "ankou": 0,
            "kraken": 0,
            "ghost": 0,
            "doll": 0,
            "zonbe": 0,
            "zonbes": 0,
            "frederika": 0,
            "chrom": 0,
            "fairy": 0,
            "elf": 0,
            "tfairy": 0,
            "fairys": 0,
            "sylph": 0,
            "c_dryad": 0,
            "taran": 0,
            "mino": 0,
            "sasori": 0,
            "lamp": 0,
            "mummy": 0,
            "kobura": 0,
            "lamias": 0,
            "sphinx": 0,
            "suckvore": 0,
            "wormv": 0,
            "ironmaiden": 0,
            "lily": 0,
            "arizigoku": 0,
            "sandw": 0,
            "gnome": 0,

            # Часть 2
            "centa": 0,
            "kaeru": 0,
            "alraune": 0,
            "dullahan": 0,
            "cerberus": 0,
            "alma_elma2": 0,
            "yukionna": 0,
            "nekomata": 0,
            "samuraielf": 0,
            "kunoitielf": 0,
            "yamatanooroti": 0,
            "moss": 0,
            "mosquito": 0,
            "imomusi": 0,
            "a_looty": 0,
            "a_parasol": 0,
            "a_prison": 0,
            "mukade": 0,
            "kaiko": 0,
            "suzumebati": 0,
            "queenbee": 0,
            "a_alm": 0,
            "a_vore": 0,
            "a_emp": 0,
            "dorothy": 0,
            "rafi": 0,
            "dina": 0,
            "jelly": 0,
            "blob": 0,
            "slime_blue": 0,
            "slime_green": 0,
            "erubetie1": 0,
            "undine": 0,
            "kamakiri": 0,
            "scylla": 0,
            "medusa": 0,
            "golem": 0,
            "madgolem": 0,
            "artm": 0,
            "ant": 0,
            "queenant": 0,
            "maccubus": 0,
            "minccubus": 0,
            "renccubus": 0,
            "succubus": 0,
            "witchs": 0,
            "lilith": 0,
            "madaminsect": 0,
            "madamumbrella": 0,
            "maidscyulla": 0,
            "emily": 0,
            "cassandra": 0,
            "yougan": 0,
            "basilisk": 0,
            "dragon": 0,
            "salamander": 0,
            "granberia3": 0,
            "kani2": 0,
            "hitode": 0,
            "dagon": 0,
            "hatibi": 0,
            "poseidones" : 0,
            "seiren": 0,
            "inp": 0,
            "beelzebub": 0,
            "trickfairy": 0,
            "queenfairy": 0,
            "queenelf": 0,
            "saraevil": 0,
            "wyvern": 0,
            "kyoryuu": 0,
            "c_beast": 0,
            "c_dryad_vore": 0,
            "behemoth": 0,
            "vampire": 0,
            "esuccubus": 0,
            "gigantweapon": 0,
            "alma_elma3": 0,
            "tamamo2": 0,
            "erubetie2": 0,
            "granberia4": 0,
            "alice3": 0,

            # Часть 3
            "cupid": 0,
            "valkyrie": 0,
            "ariel": 0,
            "c_s2": 0,
            "c_a3": 0,
            "stein1": 0,
            "trinity": 0,
            "ranael": 0,
            "c_tangh": 0,
            "nagael": 0,
            "mariel": 0,
            "angels": 0,
            "c_tentacle": 0,
            "c_medulahan": 0,
            "sisterlamia": 0,
            "c_bug": 0,
            "muzukiel": 0,
            "mermaid": 0,
            "g_mermaid": 0,
            "ningyohime": 0,
            "queenmermaid": 0,
            "traptemis": 0,
            "shadow": 0,
            "gaistvine": 0,
            "chrom2": 0,
            "berryel": 0,
            "revel": 0,
            "alakneload": 0,
            "tutigumo": 0,
            "kumonomiko": 0,
            "c_homunculus": 0,
            "ironmaiden_k": 0,
            "lusia": 0,
            "silkiel": 0,
            "endiel": 0,
            "trooperloid": 0,
            "knightloid": 0,
            "akaname": 0,
            "mikolamia": 0,
            "sirohebisama": 0,
            "walraune": 0,
            "dryad": 0,
            "queenalraune": 0,
            "slimelord": 0,
            "eggel": 0,
            "carmilla": 0,
            "elisabeth": 0,
            "queenvanpire": 0,
            "inps": 0,
            "narcubus": 0,
            "eva": 0,
            "lamianloid": 0,
            "assassinloid": 0,
            "yomotu": 0,
            "wormiel": 0,
            "fermesara": 0,
            "angelghoul": 0,
            "dragonzonbe": 0,
            "doppele": 0,
            "shirom": 0,
            "drainplant": 0,
            "drainloid": 0,
            "replicant": 0,
            "laplace": 0,
            "alice8th1": 0,
            "gargoyle": 0,
            "hainu": 0,
            "amphis": 0,
            "tukuyomi": 0,
            "arcen": 0,
            "rapun": 0,
            "heavensgate": 0,
            "eden": 0,
            "stein2": 0,
            "alice8th2": 0,
            "alice8th3": 0,
            "alice8th4": 0,
            "ilias3": 0,
            "ilias4": 0,
            "kezyorou": 0,
            "cirque1": 0,
            "alice15th": 0,

        }

    # счетчик навыков использованных монстрами(ключ: [сколько раз использовано, сколько раз кончил от навыка])
    if persistent.skills is None:
        persistent.skills = {3001: [0, 0],
                            3002: [0, 0],
                            3003: [0, 0],
                            3004: [0, 0],
                            3005: [0, 0],
                            3006: [0, 0],
                            3007: [0, 0],
                            3008: [0, 0],
                            3009: [0, 0],
                            3010: [0, 0],
                            3011: [0, 0],
                            3012: [0, 0],
                            3013: [0, 0],
                            3014: [0, 0],
                            3015: [0, 0],
                            3016: [0, 0],
                            3017: [0, 0],
                            3018: [0, 0],
                            3019: [0, 0],
                            3020: [0, 0],
                            3021: [0, 0],
                            3022: [0, 0],
                            3023: [0, 0],
                            3024: [0, 0],
                            3025: [0, 0],
                            3026: [0, 0],
                            3027: [0, 0],
                            3028: [0, 0],
                            3029: [0, 0],
                            3030: [0, 0],
                            3031: [0, 0],
                            3032: [0, 0],
                            3033: [0, 0],
                            3034: [0, 0],
                            3035: [0, 0],
                            3036: [0, 0],
                            3037: [0, 0],
                            3038: [0, 0],
                            3039: [0, 0],
                            3040: [0, 0],
                            3041: [0, 0],
                            3042: [0, 0],
                            3043: [0, 0],
                            3044: [0, 0],
                            3045: [0, 0],
                            3046: [0, 0],
                            3047: [0, 0],
                            3048: [0, 0],
                            3049: [0, 0],
                            3050: [0, 0],
                            3051: [0, 0],
                            3052: [0, 0],
                            3053: [0, 0],
                            3054: [0, 0],
                            3055: [0, 0],
                            3056: [0, 0],
                            3057: [0, 0],
                            3058: [0, 0],
                            3059: [0, 0],
                            3060: [0, 0],
                            3061: [0, 0],
                            3062: [0, 0],
                            3063: [0, 0],
                            3064: [0, 0],
                            3065: [0, 0],
                            3066: [0, 0],
                            3067: [0, 0],
                            3068: [0, 0],
                            3069: [0, 0],
                            3070: [0, 0],
                            3071: [0, 0],
                            3072: [0, 0],
                            3073: [0, 0],
                            3074: [0, 0],
                            3075: [0, 0],
                            3076: [0, 0],
                            3077: [0, 0],
                            3078: [0, 0],
                            3079: [0, 0],
                            3080: [0, 0],
                            3081: [0, 0],
                            3082: [0, 0],
                            3083: [0, 0],
                            3084: [0, 0],
                            3085: [0, 0],
                            3086: [0, 0],
                            3087: [0, 0],
                            3088: [0, 0],
                            3089: [0, 0],
                            3090: [0, 0],
                            3091: [0, 0],
                            3092: [0, 0],
                            3093: [0, 0],
                            3094: [0, 0],
                            3095: [0, 0],
                            3096: [0, 0],
                            3097: [0, 0],
                            3098: [0, 0],
                            3099: [0, 0],
                            3100: [0, 0],
                            3101: [0, 0],
                            3102: [0, 0],
                            3103: [0, 0],
                            3104: [0, 0],
                            3105: [0, 0],
                            3106: [0, 0],
                            3107: [0, 0],
                            3108: [0, 0],
                            3109: [0, 0],
                            3110: [0, 0],
                            3111: [0, 0],
                            3112: [0, 0],
                            3113: [0, 0],
                            3114: [0, 0],
                            3115: [0, 0],
                            3116: [0, 0],
                            3117: [0, 0],
                            3118: [0, 0],
                            3119: [0, 0],
                            3120: [0, 0],
                            3121: [0, 0],
                            3122: [0, 0],
                            3123: [0, 0],
                            3124: [0, 0],
                            3125: [0, 0],
                            3126: [0, 0],
                            3127: [0, 0],
                            3128: [0, 0],
                            3129: [0, 0],
                            3130: [0, 0],
                            3131: [0, 0],
                            3132: [0, 0],
                            3133: [0, 0],
                            3134: [0, 0],
                            3135: [0, 0],
                            3136: [0, 0],
                            3137: [0, 0],
                            3138: [0, 0],
                            3139: [0, 0],
                            3140: [0, 0],
                            3141: [0, 0],
                            3142: [0, 0],
                            3143: [0, 0],
                            3144: [0, 0],
                            3145: [0, 0],
                            3146: [0, 0],
                            3147: [0, 0],
                            3148: [0, 0],
                            3149: [0, 0],
                            3150: [0, 0],
                            3151: [0, 0],
                            3152: [0, 0],
                            3153: [0, 0],
                            3154: [0, 0],
                            3155: [0, 0],
                            3156: [0, 0],
                            3157: [0, 0],
                            3158: [0, 0],
                            3159: [0, 0],
                            3160: [0, 0],
                            3161: [0, 0],
                            3162: [0, 0],
                            3163: [0, 0],
                            3164: [0, 0],
                            3165: [0, 0],
                            3166: [0, 0],
                            3167: [0, 0],
                            3168: [0, 0],
                            3169: [0, 0],
                            3170: [0, 0],
                            3171: [0, 0],
                            3172: [0, 0],
                            3173: [0, 0],
                            3174: [0, 0],
                            3175: [0, 0],
                            3176: [0, 0],
                            3177: [0, 0],
                            3178: [0, 0],
                            3179: [0, 0],
                            3180: [0, 0],
                            3181: [0, 0],
                            3182: [0, 0],
                            3183: [0, 0],
                            3184: [0, 0],
                            3185: [0, 0],
                            3186: [0, 0],
                            3187: [0, 0],
                            3188: [0, 0],
                            3189: [0, 0],
                            3190: [0, 0],
                            3191: [0, 0],
                            3192: [0, 0],
                            3193: [0, 0],
                            3194: [0, 0],
                            3195: [0, 0],
                            3196: [0, 0],
                            3197: [0, 0],
                            3198: [0, 0],
                            3199: [0, 0],
                            3200: [0, 0],
                            3201: [0, 0],
                            3202: [0, 0],
                            3203: [0, 0],
                            3204: [0, 0],
                            3205: [0, 0],
                            3206: [0, 0],
                            3207: [0, 0],
                            3208: [0, 0],
                            3209: [0, 0],
                            3210: [0, 0],
                            3211: [0, 0],
                            3212: [0, 0],
                            3213: [0, 0],
                            3214: [0, 0],
                            3215: [0, 0],
                            3216: [0, 0],
                            3217: [0, 0],
                            3218: [0, 0],
                            3219: [0, 0],
                            3220: [0, 0],
                            3221: [0, 0],
                            3222: [0, 0],
                            3223: [0, 0],
                            3224: [0, 0],
                            3225: [0, 0],
                            3226: [0, 0],
                            3227: [0, 0],
                            3228: [0, 0],
                            3229: [0, 0],
                            3230: [0, 0],
                            3231: [0, 0],
                            3232: [0, 0],
                            3233: [0, 0],
                            3234: [0, 0],
                            3235: [0, 0],
                            3236: [0, 0],
                            3237: [0, 0],
                            3238: [0, 0],
                            3239: [0, 0],
                            3240: [0, 0],
                            3241: [0, 0],
                            3242: [0, 0],
                            3243: [0, 0],
                            3244: [0, 0],
                            3245: [0, 0],
                            3246: [0, 0],
                            3247: [0, 0],
                            3248: [0, 0],
                            3249: [0, 0],
                            3250: [0, 0],
                            3251: [0, 0],
                            3252: [0, 0],
                            3253: [0, 0],
                            3254: [0, 0],
                            3255: [0, 0],
                            3256: [0, 0],
                            3257: [0, 0],
                            3258: [0, 0],
                            3259: [0, 0],
                            3260: [0, 0],
                            3261: [0, 0],
                            3262: [0, 0],
                            3263: [0, 0],
                            3264: [0, 0],
                            3265: [0, 0],
                            3266: [0, 0],
                            3267: [0, 0],
                            3268: [0, 0],
                            3269: [0, 0],
                            3270: [0, 0],
                            3271: [0, 0],
                            3272: [0, 0],
                            3273: [0, 0],
                            3274: [0, 0],
                            3275: [0, 0],
                            3276: [0, 0],
                            3277: [0, 0],
                            3278: [0, 0],
                            3279: [0, 0],
                            3280: [0, 0],
                            3281: [0, 0],
                            3282: [0, 0],
                            3283: [0, 0],
                            3284: [0, 0],
                            3285: [0, 0],
                            3286: [0, 0],
                            3287: [0, 0],
                            3288: [0, 0],
                            3289: [0, 0],
                            3290: [0, 0],
                            3291: [0, 0],
                            3292: [0, 0],
                            3293: [0, 0],
                            3294: [0, 0],
                            3295: [0, 0],
                            3296: [0, 0],
                            3297: [0, 0],
                            3298: [0, 0],
                            3299: [0, 0],
                            3300: [0, 0],
                            3301: [0, 0],
                            3302: [0, 0],
                            3303: [0, 0],
                            3304: [0, 0],
                            3305: [0, 0],
                            3306: [0, 0],
                            3307: [0, 0],
                            3308: [0, 0],
                            3309: [0, 0],
                            3310: [0, 0],
                            3311: [0, 0],
                            3312: [0, 0],
                            3313: [0, 0],
                            3314: [0, 0],
                            3315: [0, 0],
                            3316: [0, 0],
                            3317: [0, 0],
                            3318: [0, 0],
                            3318: [0, 0],
                            3319: [0, 0],
                            3319: [0, 0],
                            3320: [0, 0],
                            3321: [0, 0],
                            3321: [0, 0],
                            3322: [0, 0],
                            3323: [0, 0],
                            3324: [0, 0],
                            3325: [0, 0],
                            3326: [0, 0],
                            3327: [0, 0],
                            3328: [0, 0],
                            3329: [0, 0],
                            3330: [0, 0],
                            3331: [0, 0],
                            3332: [0, 0],
                            3333: [0, 0],
                            3334: [0, 0],
                            3335: [0, 0],
                            3336: [0, 0],
                            3337: [0, 0],
                            3338: [0, 0],
                            3339: [0, 0],
                            3340: [0, 0],
                            3341: [0, 0],
                            3342: [0, 0],
                            3343: [0, 0],
                            3344: [0, 0],
                            3345: [0, 0],
                            3346: [0, 0],
                            3347: [0, 0],
                            3348: [0, 0],
                            3349: [0, 0],
                            3350: [0, 0],
                            3351: [0, 0],
                            3352: [0, 0],
                            3353: [0, 0],
                            3354: [0, 0],
                            3355: [0, 0],
                            3356: [0, 0],
                            3357: [0, 0],
                            3358: [0, 0],
                            3359: [0, 0],
                            3360: [0, 0],
                            3361: [0, 0],
                            3362: [0, 0],
                            3363: [0, 0],
                            3364: [0, 0],
                            3365: [0, 0],
                            3366: [0, 0],
                            3367: [0, 0],
                            3368: [0, 0],
                            3369: [0, 0],
                            3370: [0, 0],
                            3371: [0, 0],
                            3372: [0, 0],
                            3373: [0, 0],
                            3374: [0, 0],
                            3375: [0, 0],
                            3376: [0, 0],
                            3377: [0, 0],
                            3378: [0, 0],
                            3379: [0, 0],
                            3380: [0, 0],
                            3381: [0, 0],
                            3382: [0, 0],
                            3383: [0, 0],
                            3384: [0, 0],
                            3384: [0, 0],
                            3384: [0, 0],
                            3385: [0, 0],
                            3385: [0, 0],
                            3385: [0, 0],
                            3386: [0, 0],
                            3386: [0, 0],
                            3386: [0, 0],
                            3387: [0, 0],
                            3387: [0, 0],
                            3387: [0, 0],
                            3388: [0, 0],
                            3388: [0, 0],
                            3388: [0, 0],
                            3389: [0, 0],
                            3389: [0, 0],
                            3389: [0, 0],
                            3390: [0, 0],
                            3390: [0, 0],
                            3390: [0, 0],
                            3391: [0, 0],
                            3392: [0, 0],
                            3393: [0, 0],
                            3394: [0, 0],
                            3395: [0, 0],
                            3396: [0, 0],
                            3397: [0, 0],
                            3398: [0, 0],
                            3399: [0, 0],
                            3400: [0, 0],
                            3401: [0, 0],
                            3402: [0, 0],
                            3403: [0, 0],
                            3404: [0, 0],
                            3405: [0, 0],
                            3406: [0, 0],
                            3407: [0, 0],
                            3408: [0, 0],
                            3409: [0, 0],
                            3410: [0, 0],
                            3411: [0, 0],
                            3412: [0, 0],
                            3413: [0, 0],
                            3414: [0, 0],
                            3415: [0, 0],
                            3416: [0, 0],
                            3417: [0, 0],
                            3418: [0, 0],
                            3419: [0, 0],
                            3420: [0, 0],
                            3421: [0, 0],
                            3422: [0, 0],
                            3423: [0, 0],
                            3424: [0, 0],
                            3425: [0, 0],
                            3426: [0, 0],
                            3427: [0, 0],
                            3428: [0, 0],
                            3429: [0, 0],
                            3430: [0, 0],
                            3431: [0, 0],
                            3432: [0, 0],
                            3433: [0, 0],
                            3434: [0, 0],
                            3435: [0, 0],
                            3436: [0, 0],
                            3437: [0, 0],
                            3438: [0, 0],
                            3439: [0, 0],
                            3440: [0, 0],
                            3441: [0, 0],
                            3442: [0, 0],
                            3443: [0, 0],
                            3444: [0, 0],
                            3445: [0, 0],
                            3446: [0, 0],
                            3447: [0, 0],
                            3448: [0, 0],
                            3449: [0, 0],
                            3450: [0, 0],
                            3451: [0, 0],
                            3452: [0, 0],
                            3453: [0, 0],
                            3454: [0, 0],
                            3455: [0, 0],
                            3456: [0, 0],
                            3457: [0, 0],
                            3458: [0, 0],
                            3459: [0, 0],
                            3460: [0, 0],
                            3461: [0, 0],
                            3462: [0, 0],
                            3463: [0, 0],
                            3464: [0, 0],
                            3465: [0, 0],
                            3466: [0, 0],
                            3467: [0, 0],
                            3468: [0, 0],
                            3469: [0, 0],
                            3470: [0, 0],
                            3471: [0, 0],
                            3472: [0, 0],
                            3473: [0, 0],
                            3474: [0, 0],
                            3475: [0, 0],
                            3476: [0, 0],
                            3477: [0, 0],
                            3478: [0, 0],
                            3479: [0, 0],
                            3480: [0, 0],
                            3481: [0, 0],
                            3482: [0, 0],
                            3483: [0, 0],
                            3484: [0, 0],
                            3485: [0, 0],
                            3486: [0, 0],
                            3487: [0, 0],
                            3488: [0, 0],
                            3489: [0, 0],
                            3490: [0, 0],
                            3491: [0, 0],
                            3492: [0, 0],
                            3493: [0, 0],
                            3494: [0, 0],
                            3495: [0, 0],
                            3496: [0, 0],
                            3497: [0, 0],
                            3498: [0, 0],
                            3499: [0, 0],
                            3500: [0, 0],
                            3501: [0, 0],
                            3502: [0, 0],
                            3503: [0, 0],
                            3504: [0, 0],
                            3505: [0, 0],
                            3506: [0, 0],
                            3507: [0, 0],
                            3508: [0, 0],
                            3509: [0, 0],
                            3510: [0, 0],
                            3511: [0, 0],
                            3512: [0, 0],
                            3513: [0, 0],
                            3514: [0, 0],
                            3515: [0, 0],
                            3516: [0, 0],
                            3517: [0, 0],
                            3518: [0, 0],
                            3519: [0, 0],
                            3520: [0, 0],
                            3521: [0, 0],
                            3522: [0, 0],
                            3523: [0, 0],
                            3524: [0, 0],
                            3525: [0, 0],
                            3526: [0, 0],
                            3527: [0, 0],
                            3528: [0, 0],
                            3529: [0, 0],
                            3530: [0, 0],
                            3531: [0, 0],
                            3532: [0, 0],
                            3533: [0, 0],
                            3534: [0, 0],
                            3535: [0, 0],
                            3536: [0, 0],
                            3537: [0, 0],
                            3538: [0, 0],
                            3539: [0, 0],
                            3540: [0, 0],
                            3541: [0, 0],
                            3542: [0, 0],
                            3543: [0, 0],
                            3544: [0, 0],
                            3545: [0, 0],
                            3546: [0, 0],
                            3547: [0, 0],
                            3548: [0, 0],
                            3549: [0, 0],
                            3550: [0, 0],
                            3551: [0, 0],
                            3552: [0, 0],
                            3553: [0, 0],
                            3554: [0, 0],
                            3555: [0, 0],
                            3556: [0, 0],
                            3557: [0, 0],
                            3558: [0, 0],
                            3559: [0, 0],
                            3560: [0, 0],
                            3561: [0, 0],
                            3562: [0, 0],
                            3563: [0, 0],
                            3564: [0, 0],
                            3565: [0, 0],
                            3566: [0, 0],
                            3567: [0, 0],
                            3568: [0, 0],
                            3569: [0, 0],
                            3570: [0, 0],
                            3571: [0, 0],
                            3572: [0, 0],
                            3573: [0, 0],
                            3574: [0, 0],
                            3575: [0, 0],
                            3576: [0, 0],
                            3577: [0, 0],
                            3578: [0, 0],
                            3579: [0, 0],
                            3580: [0, 0],
                            3581: [0, 0],
                            3582: [0, 0],
                            3583: [0, 0],
                            3584: [0, 0],
                            3585: [0, 0],
                            3586: [0, 0],
                            3587: [0, 0],
                            3588: [0, 0],
                            3589: [0, 0],
                            3590: [0, 0],
                            3591: [0, 0],
                            3592: [0, 0],
                            3593: [0, 0],
                            3594: [0, 0],
                            3595: [0, 0],
                            3596: [0, 0],
                            3597: [0, 0],
                            3598: [0, 0],
                            3599: [0, 0],
                            3600: [0, 0],
                            3600: [0, 0],
                            3601: [0, 0],
                            3602: [0, 0],
                            3603: [0, 0],
                            3604: [0, 0],
                            3605: [0, 0],
                            3606: [0, 0],
                            3607: [0, 0],
                            3608: [0, 0],
                            3609: [0, 0],
                            3610: [0, 0],
                            3611: [0, 0],
                            3612: [0, 0],
                            3613: [0, 0],
                            3614: [0, 0],
                            3615: [0, 0],
                            3616: [0, 0],
                            3617: [0, 0],
                            3618: [0, 0],
                            3619: [0, 0],
                            3620: [0, 0],
                            3621: [0, 0],
                            3622: [0, 0],
                            3623: [0, 0],
                            3624: [0, 0],
                            3625: [0, 0],
                            3626: [0, 0],
                            3627: [0, 0],
                            3628: [0, 0],
                            3629: [0, 0],
                            3630: [0, 0],
                            3631: [0, 0],
                            3632: [0, 0],
                            3633: [0, 0],
                            3634: [0, 0],
                            3635: [0, 0],
                            3636: [0, 0],
                            3637: [0, 0],
                            3638: [0, 0],
                            3639: [0, 0],
                            3640: [0, 0],
                            3641: [0, 0],
                            3642: [0, 0],
                            3643: [0, 0],
                            3644: [0, 0],
                            3645: [0, 0],
                            3646: [0, 0],
                            3647: [0, 0],
                            3648: [0, 0],
                            3649: [0, 0],
                            3650: [0, 0],
                            3651: [0, 0],
                            3652: [0, 0],
                            3653: [0, 0],
                            3654: [0, 0],
                            3655: [0, 0],
                            3656: [0, 0],
                            3657: [0, 0],
                            3658: [0, 0],
                            3659: [0, 0],
                            3660: [0, 0],
                            3661: [0, 0],
                            3662: [0, 0],
                            3663: [0, 0],
                            3664: [0, 0],
                            3665: [0, 0],
                            3666: [0, 0],
                            3667: [0, 0],
                            3668: [0, 0],
                            3669: [0, 0],
                            3670: [0, 0],
                            3671: [0, 0],
                            3672: [0, 0],
                            3673: [0, 0],
                            3674: [0, 0],
                            3675: [0, 0],
                            3676: [0, 0],
                            3677: [0, 0],
                            3678: [0, 0],
                            3679: [0, 0],
                            3680: [0, 0],
                            3681: [0, 0],
                            3682: [0, 0],
                            3683: [0, 0],
                            3684: [0, 0],
                            3685: [0, 0],
                            3686: [0, 0],
                            3687: [0, 0],
                            3688: [0, 0],
                            3689: [0, 0],
                            3690: [0, 0],
                            3691: [0, 0],
                            3692: [0, 0],
                            3693: [0, 0],
                            3694: [0, 0],
                            3695: [0, 0],
                            3696: [0, 0],
                            3697: [0, 0],
                            3698: [0, 0],
                            3699: [0, 0],
                            3700: [0, 0],
                            3701: [0, 0],
                            3702: [0, 0],
                            3703: [0, 0],
                            3704: [0, 0],
                            3705: [0, 0],
                            3706: [0, 0],
                            3707: [0, 0],
                            3708: [0, 0],
                            3709: [0, 0],
                            3710: [0, 0],
                            3711: [0, 0],
                            3712: [0, 0],
                            3713: [0, 0],
                            3714: [0, 0],
                            3715: [0, 0],
                            3716: [0, 0],
                            3717: [0, 0],
                            3718: [0, 0],
                            3719: [0, 0],
                            3720: [0, 0],
                            3721: [0, 0],
                            3722: [0, 0],
                            3723: [0, 0],
                            3724: [0, 0],
                            3725: [0, 0],
                            3726: [0, 0],
                            3727: [0, 0],
                            3728: [0, 0],
                            3729: [0, 0],
                            3730: [0, 0],
                            3731: [0, 0],
                            3732: [0, 0],
                            3733: [0, 0],
                            3734: [0, 0],
                            3735: [0, 0],
                            3736: [0, 0],
                            3737: [0, 0],
                            3738: [0, 0],
                            3763: [0, 0],
                            3764: [0, 0],
                            3765: [0, 0],
                            3766: [0, 0],
                            3767: [0, 0],
                            3768: [0, 0],
                            3769: [0, 0],
                            3770: [0, 0],
                            3771: [0, 0],
                            3772: [0, 0],
                            3773: [0, 0],
                            3774: [0, 0],
                            3775: [0, 0],
                            3776: [0, 0],
                            3777: [0, 0],
                            3778: [0, 0],
                            3779: [0, 0],
                            3780: [0, 0],
                            3781: [0, 0],
                            3782: [0, 0],
                            3783: [0, 0],
                            3784: [0, 0],
                            3785: [0, 0],
                            3786: [0, 0],
                            3787: [0, 0],
                            3788: [0, 0],
                            3789: [0, 0],
                            3790: [0, 0],
                            3791: [0, 0],
                            3792: [0, 0],
                            3793: [0, 0],
                            3794: [0, 0],
                            3795: [0, 0],
                            3796: [0, 0],
                            3797: [0, 0],
                            3798: [0, 0],
                            3799: [0, 0],
                            3800: [0, 0],
                            3801: [0, 0],
                            3802: [0, 0],
                            3803: [0, 0],
                            3804: [0, 0],
                            3805: [0, 0],
                            3806: [0, 0],
                            3807: [0, 0],
                            3808: [0, 0],
                            3809: [0, 0],
                            3810: [0, 0],
                            3811: [0, 0],
                            3812: [0, 0],
                            3813: [0, 0],
                            3814: [0, 0],
                            3815: [0, 0],
                            3816: [0, 0],
                            3817: [0, 0],
                            3818: [0, 0],
                            3819: [0, 0],
                            3820: [0, 0],
                            3821: [0, 0],
                            3822: [0, 0],
                            3823: [0, 0],
                            3824: [0, 0],
                            3825: [0, 0],
                            3826: [0, 0],
                            3827: [0, 0],
                            3828: [0, 0],
                            3829: [0, 0],
                            3830: [0, 0],
                            3831: [0, 0],
                            3832: [0, 0],
                            3833: [0, 0],
                            3834: [0, 0],
                            3835: [0, 0],
                            3836: [0, 0],
                            3837: [0, 0],
                            3838: [0, 0],
                            3839: [0, 0],
                            3840: [0, 0],
                            3841: [0, 0],
                            3842: [0, 0],
                            3843: [0, 0],
                            3844: [0, 0],
                            3845: [0, 0],
                            3846: [0, 0],
                            3847: [0, 0],
                            3848: [0, 0],
                            3849: [0, 0],
                            3850: [0, 0],
                            3851: [0, 0],
                            3852: [0, 0],
                            3853: [0, 0],
                            3854: [0, 0],
                            3855: [0, 0],
                            3856: [0, 0],
                            3857: [0, 0],
                            3858: [0, 0],
                            3859: [0, 0],
                            3860: [0, 0],
                            3861: [0, 0],
                            3862: [0, 0],
                            3863: [0, 0],
                            3864: [0, 0],
                            3865: [0, 0],
                            3866: [0, 0],
                            3867: [0, 0],
                            3868: [0, 0],
                            3869: [0, 0],
                            3870: [0, 0],
                            3871: [0, 0],
                            3872: [0, 0],
                            3873: [0, 0],
                            3874: [0, 0],
                            3875: [0, 0],
                            3876: [0, 0],
                            3877: [0, 0],
                            3878: [0, 0],
                            3879: [0, 0],
                            3880: [0, 0],
                            3881: [0, 0],
                            3882: [0, 0],
                            3883: [0, 0],
                            3884: [0, 0],
                            3885: [0, 0],
                            3886: [0, 0],
                            3887: [0, 0],
                            3888: [0, 0],
                            3889: [0, 0],
                            3890: [0, 0],
                            3891: [0, 0],
                            3892: [0, 0],
                            3893: [0, 0],
                            3894: [0, 0],
                            3895: [0, 0],
                            3896: [0, 0],
                            3897: [0, 0],
                            3898: [0, 0],
                            3899: [0, 0],
                            3900: [0, 0],
                            3901: [0, 0],
                            3902: [0, 0],
                            3903: [0, 0],
                            3904: [0, 0],
                            3905: [0, 0],
                            3906: [0, 0],
                            3907: [0, 0],
                            3908: [0, 0],
                            3909: [0, 0],
                            3910: [0, 0],
                            3911: [0, 0],
                            3912: [0, 0],
                            3913: [0, 0],
                            3914: [0, 0],
                            3915: [0, 0],
                            3916: [0, 0],
                            3917: [0, 0],
                            3918: [0, 0],
                            3919: [0, 0],
                            3920: [0, 0],
                            3921: [0, 0],
                            3922: [0, 0],
                            3923: [0, 0],
                            3924: [0, 0],
                            3925: [0, 0],
                            3926: [0, 0],
                            3927: [0, 0],
                            3928: [0, 0],
                            3929: [0, 0],
                            3930: [0, 0],
                            3931: [0, 0],
                            3932: [0, 0],
                            3933: [0, 0],
                            3934: [0, 0],
                            3935: [0, 0],
                            3936: [0, 0],
                            3937: [0, 0],
                            3938: [0, 0],
                            3939: [0, 0],
                            3940: [0, 0],
                            3941: [0, 0],
                            3942: [0, 0],
                            3943: [0, 0],
                            3944: [0, 0],
                            3945: [0, 0],
                            3946: [0, 0],
                            3947: [0, 0],
                            3948: [0, 0],
                            3949: [0, 0],
                            3950: [0, 0],
                            3951: [0, 0],
                            3952: [0, 0],
                            3953: [0, 0],
                            3954: [0, 0],
                            3955: [0, 0],
                            3956: [0, 0],
                            3957: [0, 0],
                            3958: [0, 0],
                            3959: [0, 0],
                            3960: [0, 0],
                            3961: [0, 0],
                            3962: [0, 0],
                            3963: [0, 0],
                            3964: [0, 0],
                            3965: [0, 0],
                            3966: [0, 0],
                            3967: [0, 0],
                            3968: [0, 0],
                            3969: [0, 0],
                            3970: [0, 0],
                            3971: [0, 0],
                            3972: [0, 0],
                            3973: [0, 0],
                            3974: [0, 0],
                            3975: [0, 0],
                            3976: [0, 0],
                            3977: [0, 0],
                            3978: [0, 0],
                            3979: [0, 0],
                            3980: [0, 0],
                            3981: [0, 0],
                            3982: [0, 0],
                            3985: [0, 0],
                            3986: [0, 0],
                            3987: [0, 0],
                            3988: [0, 0],
                            3989: [0, 0],
                            3990: [0, 0],
                            3991: [0, 0],
                            3992: [0, 0],
                            3993: [0, 0],
                            3994: [0, 0],
                            3995: [0, 0],
                            3996: [0, 0],
                            3997: [0, 0],
                            3998: [0, 0],
                            4000: [0, 0],
                            4001: [0, 0],
                            4002: [0, 0],
                            4003: [0, 0],
                            4004: [0, 0],
                            4005: [0, 0],
                            4006: [0, 0],
                            4007: [0, 0],
                            4008: [0, 0],
                            4009: [0, 0],
                            4010: [0, 0],
                            4011: [0, 0],
                            4012: [0, 0],
                            4013: [0, 0],
                            4014: [0, 0],
                            4015: [0, 0],
                            4016: [0, 0],
                            4017: [0, 0],
                            4018: [0, 0],
                            4019: [0, 0],
                            4020: [0, 0],
                            4021: [0, 0],
                            4022: [0, 0],
                            4023: [0, 0],
                            4024: [0, 0],
                            4025: [0, 0],
                            4026: [0, 0],
                            4027: [0, 0],
                            4028: [0, 0],
                            4029: [0, 0],
                            4030: [0, 0],
                            4031: [0, 0],
                            4032: [0, 0],
                            4033: [0, 0],
                            4034: [0, 0],
                            4035: [0, 0],
                            4036: [0, 0],
                            4037: [0, 0],
                            4038: [0, 0],
                            4039: [0, 0],
                            4040: [0, 0],
                            4041: [0, 0],
                            4042: [0, 0],
                            4043: [0, 0],
                            4044: [0, 0],
                            4045: [0, 0],
                            4046: [0, 0],
                            4047: [0, 0],
                            4048: [0, 0],
                            4049: [0, 0],
                            4050: [0, 0],
                            4051: [0, 0],
                            4052: [0, 0],
                            4053: [0, 0],
                            4054: [0, 0],
                            4055: [0, 0],
                            4056: [0, 0],
                            4057: [0, 0],
                            4058: [0, 0],
                            4059: [0, 0],
                            4060: [0, 0],
                            4061: [0, 0],
                            4062: [0, 0],
                            4063: [0, 0],
                            4064: [0, 0],
                            4065: [0, 0],
                            4066: [0, 0],
                            4067: [0, 0],
                            4068: [0, 0],
                            4069: [0, 0],
                            4070: [0, 0],
                            4071: [0, 0],
                            4072: [0, 0],
                            4073: [0, 0],
                            4074: [0, 0],
                            4075: [0, 0],
                            4076: [0, 0],
                            4077: [0, 0],
                            4078: [0, 0],
                            4079: [0, 0],
                            4080: [0, 0],
                            4081: [0, 0],
                            4082: [0, 0],
                            4083: [0, 0],
                            4084: [0, 0],
                            4085: [0, 0],
                            4086: [0, 0],
                            4087: [0, 0],
                            4088: [0, 0],
                            4089: [0, 0],
                            4090: [0, 0],
                            4091: [0, 0],
                            4092: [0, 0],
                            4093: [0, 0],
                            4094: [0, 0],
                            4095: [0, 0],
                            4096: [0, 0],
                            4097: [0, 0],
                            4098: [0, 0],
                            4099: [0, 0],
                            4100: [0, 0],
                            4101: [0, 0],
                            4102: [0, 0],
                            4103: [0, 0],
                            4104: [0, 0],
                            4105: [0, 0],
                            4106: [0, 0],
                            4107: [0, 0],
                            4108: [0, 0],
                            4109: [0, 0],
                            4110: [0, 0],
                            4111: [0, 0],
                            4112: [0, 0],
                            4113: [0, 0],
                            4114: [0, 0],
                            4115: [0, 0],
                            4116: [0, 0],
                            4117: [0, 0],
                            4118: [0, 0],
                            4119: [0, 0],
                            4120: [0, 0],
                            4121: [0, 0],
                            4122: [0, 0],
                            4123: [0, 0],
                            4124: [0, 0],
                            4125: [0, 0],
                            4126: [0, 0],
                            4127: [0, 0],
                            4128: [0, 0],
                            4129: [0, 0],
                            4130: [0, 0],
                            4131: [0, 0],
                            4132: [0, 0],
                            4133: [0, 0],
                            4134: [0, 0],
                            4135: [0, 0],
                            4136: [0, 0]
    }

    # переменные для настройки стиля
    # вид текстового окна
    if persistent.box is None:
        persistent.box = "images/System/boxes/t_box.webp"

    # цвет текстового окна
    if persistent.box_color is None:
        persistent.box_color = "#330000"

    if persistent.who_color is None:
        persistent.who_color = "#fff"

    if persistent.who_out_color is None:
        persistent.who_out_color = "#000"

    if persistent.who_out_thickness is None:
        persistent.who_out_thickness = 2

    if persistent.who_font is None:
        persistent.who_font = "fonts/verdana.ttf"

    if persistent.who_font_size is None:
        persistent.who_font_size = 18

    if persistent.what_color is None:
        persistent.what_color = "#fff"

    if persistent.what_out_color is None:
        persistent.what_out_color = "#000"

    if persistent.what_out_thickness is None:
        persistent.what_out_thickness = 2

    if persistent.what_font is None:
        persistent.what_font = "fonts/verdana.ttf"

    if persistent.what_font_size is None:
        persistent.what_font_size = 18

    # NG+ значения

    if persistent.pedia_view is None:
        if renpy.variant("touch"):
            persistent.pedia_view = "new"
        else:
            persistent.pedia_view = "old"

    if persistent.music is None:
        persistent.music = "new"

    if persistent.granberia_hell is None:
        persistent.granberia_hell = 0

    if persistent.queenhapy_hell is None:
        persistent.queenhapy_hell = 0

    if persistent.tamamo_hell is None:
        persistent.tamamo_hell = 0

    if persistent.granberia_maxrep is None:
        persistent.granberia_maxrep = 0

    if persistent.granberia_minrep is None:
        persistent.granberia_minrep = 0

    if persistent.tamamo_maxrep is None:
        persistent.tamamo_maxrep = 0

    if persistent.starblade is None:
        persistent.starblade = 0

    if persistent.nine_moons is None:
        persistent.nine_moons = 0

    if persistent.nine_moons_won is None:
        persistent.nine_moons_won = 0

    if persistent.tamamo_knows is None:
        persistent.tamamo_knows = 0

    if persistent.ng1_achi is None:
        persistent.ng1_achi = 0

    if persistent.ng_clear is None:
        persistent.ng_clear = 0

    if persistent.lilith_hell is None:
        persistent.lilith_hell = 0

    if persistent.cupidsuccubus_debut is None:
        persistent.cupidsuccubus_debut = 0

    if persistent.lily_generator is None:
        persistent.lily_generator = 0

    if persistent.koakgigacount is None:
        persistent.koakgigacount = 0

    if persistent.tamamo_ng_unlock is None:
        persistent.tamamo_ng_unlock = 0

    if persistent.granberia_ng_unlock is None:
        persistent.granberia_ng_unlock = 0

    if persistent.queenharpy_ng_unlock is None:
        persistent.queenharpy_ng_unlock = 0

    if persistent.alma_ng_unlock is None:
        persistent.alma_ng_unlock = 0

    if persistent.sylph_ng_unlock is None:
        persistent.sylph_ng_unlock = 0

    if persistent.gnome_ng_unlock is None:
        persistent.gnome_ng_unlock = 0

    if persistent.erubetie_ng_unlock is None:
        persistent.erubetie_ng_unlock = 0

    if persistent.undine_ng_unlock is None:
        persistent.undine_ng_unlock = 0

    if persistent.salamander_ng_unlock is None:
        persistent.salamander_ng_unlock = 0

    if persistent.ng_clear is None:
        persistent.ng_clear = 0

    if persistent.NGMODE is None:
        persistent.NGMODE = True

    if persistent.inv is None:
        persistent.inv = "new"

    if persistent.testbattle is None:
        persistent.testbattle = False
