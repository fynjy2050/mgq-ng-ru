#############################################################################
# Extras menu
#
#
screen extras():
    tag menu

    add Solid("#000")

    style_group "extras"

    frame:
        align (0.5, 0.5)
        # background None

        vbox:
            textbutton "Монстропедия":
                activate_sound "audio/m_papier02.mp3"
                action ShowMenu("pedia_%s" % persistent.pedia_view, enter="ex")

            textbutton "Дневник" action ShowMenu("record", enter="ex")
            textbutton "Музыка" action ShowMenu("music_room")
            # textbutton "Auto-Save" action ShowMenu("load", enter="ex")
            textbutton "Побочные истории" action Start("spinoff_start")
            textbutton "Доп. настройки" action ShowMenu("special_config")
            #textbutton "Настройки NG+" action ShowMenu("special_config_ng")
            textbutton "Помощь" action ShowMenu("help")
            # textbutton "Credits" action Jump("credit")
            textbutton "Назад" action [Play("music", "NGDATA/bgm/gn_night.ogg"),
                                                   SetField(persistent, "show_extras_menu", False)]

    key "game_menu" action [Play("music", "NGDATA/bgm/gn_night.ogg"),
                            SetField(persistent, "show_extras_menu", False)]


style extras_frame:
    background Frame('images/System/win_bg_0.webp', 5, 5)
    padding (20, 20)

style extras_button:
    is default
    xalign .5
    background None

style extras_button_text:
    is default
    font "fonts/archiform.otf"
    outlines [(2, "#001", 0, 0)]
    size 23
    idle_color "#fff"
    hover_color "#00f"
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008", 0, 0)]

style extras_button_text:
    variant "touch"
    size 28

#############################################################################
# Help
#
# A screen that gives information about key and mouse bindings.
screen help():

    tag menu

    default device = "keyboard"

    if persistent.game_clear == 3:
        add "bg 140"
    else:
        add "bg 001"

    add "gm_bg"

    style_prefix "help"

    frame:
        background Frame("images/System/win_bg_0.webp", 5, 5)
        align (.5, .5)
        xfill True
        xmargin 10
        padding (10, 10)

        vbox:

            if device == "keyboard":
                use keyboard_help
            elif device == "mouse":
                use mouse_help
            elif device == "gamepad":
                use gamepad_help

            spacing 15

            hbox:

                textbutton _("Клавиатура") action SetScreenVariable("device", "keyboard")
                textbutton _("Мышка") action SetScreenVariable("device", "mouse")

                if GamepadExists():
                    textbutton _("Геймпад") action SetScreenVariable("device", "gamepad")

    use return_but("ex")
    use title(_("Помощь"))


screen keyboard_help():

    hbox:
        label "Enter"
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label "Space"
        text "Перейти к следующиму диалогу с пропуском выбора."

    hbox:
        label "Стрелочки"
        text "Навигация по интерфейсу."

    hbox:
        label "Escape"
        text "В главное меню."

    hbox:
        label "Ctrl"
        text "Пропустить диалоги при удержании."

    hbox:
        label "Tab"
        text "Включить/выключить пропуск диалогов."

    hbox:
        label "Page Up"
        text "Перелистнуть на более ранний диалог."

    hbox:
        label "Page Down"
        text "Перелистнуть на более поздний диалог."

    hbox:
        label "H"
        text "Спрятать интерфейса."

    hbox:
        label "S"
        text "Сделать скриншот."

    # hbox:
    #     label "V"
    #     text "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."


screen mouse_help():

    hbox:
        label _("Левая кнопка")
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label _("Средняя кнопка")
        text "Спрятать интерфейса."

    hbox:
        label _("Правая кнопка")
        text "В главное меню."

    hbox:
        label _("Колёсико вверх")
        text "Перелистнуть на более ранний диалог."

    hbox:
        label _("Колёсико вниз")
        text "Перелистнуть на более поздний диалог."


screen gamepad_help():

    hbox:
        label _("Правый триггер\nA/кнопка")
        text "Перейти к следующиму диалогу и включить интерфейс."

    hbox:
        label ("Левый триггер\nЛевый Shoulder")
        text "Перелистнуть на более ранний диалог."

    hbox:
        label _("Правый Shoulder")
        text "Перелистнуть на более поздний диалог."

    hbox:
        label _("D-Pad, стик")
        text "Навигация по интерфейсу."

    hbox:
        label _("Start, Guide")
        text "В главное меню."

    hbox:
        label _("Y/Верхняя кнопка")
        text "Спрятать интерфейса."

    textbutton _("Калибровка") action GamepadCalibrate()

style help_label is label

style help_label:
    xsize 200
    right_padding 20


screen special_config(enter="ex"):
    tag menu
    add Solid("#000")
    add "gm_bg"
    style_group "prefs"

    frame:
        background Frame("images/System/win_bg_0.webp", 5, 5)
        xfill True
        yfill True
        margin (50, 60)
        style_group "pref"

        has vbox

        frame:
            has vbox

            label "Монстропедия"

            grid 2 1:
                xsize 400
                xalign 1.0

                textbutton "Старая":
                    action SetField(persistent, "pedia_view", "old")
                textbutton "Новая":
                    action SetField(persistent, "pedia_view", "new")

        frame:
            has hbox:
                xfill True

            textbutton "Настройки NG+" action ShowMenu("special_config_ng", enter="gm")

    use title("Дополнительные настройки{#spc}")
    if in_game:
        use return_but(enter)
    else:
        use return_but("ex")

screen special_config_ng(enter="ex"):
    tag menu
    add Solid("#000")
    add "gm_bg"
    style_group "prefs"

    frame:
        background Frame("images/System/win_bg_0.webp", 5, 5)
        xfill True
        yfill True
        margin (50, 60)
        style_group "pref"

        vbox:
            frame:
                has hbox:
                    xfill True

                label "Музыка"

                grid 2 1:
                    xsize 400
                    xalign 1.0

                    textbutton "Старая музыка":
                        action SetField(persistent, "music", "old")
                    textbutton "NG+ музыка":
                        action SetField(persistent, "music", "new")

            frame:
                has hbox:
                    xfill True

                label "Инвентарь"

                grid 2 1:
                    xsize 400
                    xalign 1.0

                    textbutton "Старый":
                        action SetField(persistent, "inv", "old")
                    textbutton "Новый":
                        action SetField(persistent, "inv", "new")

            frame:
                has hbox:
                    xfill True

                textbutton "Тестовая битва":
                    action [SetField(persistent, "testbattle", True), Start()]

    use title("Настройки NG+{#spc}")
    if in_game:
        use return_but(enter)
    else:
        use return_but("ex")
