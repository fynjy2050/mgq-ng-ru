init python:

    def add_files_to_mr():
        music_files = (
            "title", "irias", "mura1", "kiki1", "battle",
            "ero1", "zukan", "hutuu1", "kiki2", "sinpi1",
            "alice1", "maou", "comi1", "field1", "yaei",
            "boss0", "sitenno", "city1", "amira", "dungeon1",
            "mura2", "boss1", "dance", "epilogue", "enrika",
            "tamamo", "hutuu2", "umi", "field3", "castle1",
            "dungeon2", "umi2", "obakeyasiki", "seireinomori", "yonseirei2",
            "yonseirei", "boss2", "sabaku", "sabasa", "castle2",
            "dungeon3", "dance2", "hutuu3", "mura3", "madou1",
            "hutuu4", "safar", "ending", "colosseum", "castle3",
            "hubuki", "yamatai", "plansect", "ruka", "izumi",
            "castle4", "kazan", "gordport", "maou2", "aqua",
            "alice2", "galda", "manotairiku", "remina", "maouzyou",
            "boss3", "boss4", "stein", "labo1", "kanasimi1",
            "kanasimi2", "kiki3", "field4", "epilogue2", "battle2",
            "maouzyou2", "sabasa2", "labo2", "labo3", "heso",
            "tower", "tenkai", "eden", "stein2", "maou3",
            "maou4", "irias2", "irias3", "epilogue3", "ending2"
            )

        for i in music_files:
            mr.add("audio/bgm/%s.ogg" % i, always_unlocked=True)

    def add_files_to_mr_ng():
        music_files = (
            "gn_night", "granberia", "granberia2", "tamamo", "boss",
            "boss0", "boss2", "boss3", "boss4", "boss5",
            "boss_adra", "field5", "field6", "field7", "field8",
            "field9", "field10", "erubetie", "comi2", "colosseum",
            "ruka2", "ruka3", "ruka4", "spirits", "spirits2",
            "iliasbattle", "hutuu5", "alice8th1", "hutuu5", "hutuu6",
            "mystery1", "shirome", "pyramid", "umi3", "porttown",
            "succubusvillage", "volcano", "volcano2", "sisters1",
            "zukan", "alma", "irias4"
            )

        for i in music_files:
            mr.add("NGDATA/bgm/%s.ogg" % i, always_unlocked=True)

    # Step 1. Create a MusicRoom instance.
    mr = MusicRoom(fadeout=1.0)

    # Step 2. Add music files.
    add_files_to_mr()
    add_files_to_mr_ng()


# Step 3. Create the music room screen.
screen music_room():
    $ ROWS = 90

    tag menu
    add "bg 140"
    add "gm_bg"

    frame:
        style_group "mr"

        has vpgrid:
            rows ROWS

            draggable True
            mousewheel True
            scrollbars "vertical"

            # Since we have scrollbars, we have to position the side, rather
            # than the vpgrid proper.
            # side_xalign 0.5
            # side_yalign 0.3

        # The buttons that play each track.
        textbutton "Главная тема" action mr.Play("audio/bgm/title.ogg")
        textbutton "Илиас" action mr.Play("audio/bgm/irias.ogg")
        textbutton "Деревня Илиас" action mr.Play("audio/bgm/mura1.ogg")
        textbutton "Бедствие 1" action mr.Play("audio/bgm/kiki1.ogg")
        textbutton "Обычная битва" action mr.Play("audio/bgm/battle.ogg")
        textbutton "H-сцена" action mr.Play("audio/bgm/ero1.ogg")
        textbutton "Монстропедия" action mr.Play("audio/bgm/zukan.ogg")
        textbutton "Мир 1" action mr.Play("audio/bgm/hutuu1.ogg")
        textbutton "Бедствие 2" action mr.Play("audio/bgm/kiki2.ogg")
        textbutton "Алиса 1" action mr.Play("audio/bgm/sinpi1.ogg")
        textbutton "Алиса 2" action mr.Play("audio/bgm/alice1.ogg")
        textbutton "Битва с Владыкой Монстров" action mr.Play("audio/bgm/maou.ogg")
        textbutton "Курьёз" action mr.Play("audio/bgm/comi1.ogg")
        textbutton "Приключение 1" action mr.Play("audio/bgm/field1.ogg")
        textbutton "Лагерь" action mr.Play("audio/bgm/yaei.ogg")
        textbutton "Boss Buildup" action mr.Play("audio/bgm/boss0.ogg")
        textbutton "Four Heavenly Knight Battle" action mr.Play("audio/bgm/sitenno.ogg")
        textbutton "Town" action mr.Play("audio/bgm/city1.ogg")
        textbutton "Amira" action mr.Play("audio/bgm/amira.ogg")
        textbutton "Dungeon 1" action mr.Play("audio/bgm/dungeon1.ogg")
        textbutton "Village" action mr.Play("audio/bgm/mura2.ogg")
        textbutton "Boss 1" action mr.Play("audio/bgm/boss1.ogg")
        textbutton "Dance 1" action mr.Play("audio/bgm/dance.ogg")
        textbutton "Epilogue" action mr.Play("audio/bgm/epilogue.ogg")
        textbutton "Enrika" action mr.Play("audio/bgm/enrika.ogg")
        textbutton "Tamamo" action mr.Play("audio/bgm/tamamo.ogg")
        textbutton "Peace 2" action mr.Play("audio/bgm/hutuu2.ogg")
        textbutton "Sea" action mr.Play("audio/bgm/umi.ogg")
        textbutton "Field 2" action mr.Play("audio/bgm/field3.ogg")
        textbutton "San Ilia Castle" action mr.Play("audio/bgm/castle1.ogg")
        textbutton "Library" action mr.Play("audio/bgm/dungeon2.ogg")
        textbutton "Under the Sea" action mr.Play("audio/bgm/umi2.ogg")
        textbutton "Haunted Mansion" action mr.Play("audio/bgm/obakeyasiki.ogg")
        textbutton "Forest of Spirits" action mr.Play("audio/bgm/seireinomori.ogg")
        textbutton "Four Spirits" action mr.Play("audio/bgm/yonseirei2.ogg")
        textbutton "Four Spirit Battle" action mr.Play("audio/bgm/yonseirei.ogg")
        textbutton "Boss 2" action mr.Play("audio/bgm/boss2.ogg")
        textbutton "Desert" action mr.Play("audio/bgm/sabaku.ogg")
        textbutton "Sabasa Castle Town" action mr.Play("audio/bgm/sabasa.ogg")
        textbutton "Sabasa Castle" action mr.Play("audio/bgm/castle2.ogg")
        textbutton "Pyramid" action mr.Play("audio/bgm/dungeon3.ogg")
        textbutton "Dance 2" action mr.Play("audio/bgm/dance2.ogg")
        textbutton "Peace 3" action mr.Play("audio/bgm/hutuu3.ogg")
        textbutton "Witch Hunt Village" action mr.Play("audio/bgm/mura3.ogg")
        textbutton "Lily's Mansion" action mr.Play("audio/bgm/madou1.ogg")
        textbutton "Sadness" action mr.Play("audio/bgm/hutuu4.ogg")
        textbutton "Safaru Ruins" action mr.Play("audio/bgm/safar.ogg")


        textbutton "Staff Roll" action mr.Play("audio/bgm/ending.ogg")
        textbutton "Colosseum" action mr.Play("audio/bgm/colosseum.ogg")
        textbutton "Grand Noah Castle" action mr.Play("audio/bgm/castle3.ogg")
        textbutton "Blizzard" action mr.Play("audio/bgm/hubuki.ogg")
        textbutton "Yamatai Village" action mr.Play("audio/bgm/yamatai.ogg")
        textbutton "Plansect Village" action mr.Play("audio/bgm/plansect.ogg")
        textbutton "Luka (Angry)" action mr.Play("audio/bgm/ruka.ogg")
        textbutton "Undine's Spring" action mr.Play("audio/bgm/izumi.ogg")
        textbutton "Grangold Castle" action mr.Play("audio/bgm/castle4.ogg")
        textbutton "Gold Volcano" action mr.Play("audio/bgm/kazan.ogg")
        textbutton "Gold Port" action mr.Play("audio/bgm/gordport.ogg")
        textbutton "Black Alice" action mr.Play("audio/bgm/maou2.ogg")
        textbutton "Serene Mind" action mr.Play("audio/bgm/aqua.ogg")
        textbutton "Alice 3" action mr.Play("audio/bgm/alice2.ogg")
        textbutton "Galda" action mr.Play("audio/bgm/galda.ogg")
        textbutton "Hellgondo" action mr.Play("audio/bgm/manotairiku.ogg")
        textbutton "Remina" action mr.Play("audio/bgm/remina.ogg")
        textbutton "Monster Lord's Castle" action mr.Play("audio/bgm/maouzyou.ogg")
        textbutton "Boss 3" action mr.Play("audio/bgm/boss3.ogg")
        textbutton "Angel Battle" action mr.Play("audio/bgm/boss4.ogg")


        textbutton "Promestein 1" action mr.Play("audio/bgm/stein.ogg")
        textbutton "Promestein's Lab" action mr.Play("audio/bgm/labo1.ogg")
        textbutton "Sorrow 2" action mr.Play("audio/bgm/kanasimi1.ogg")
        textbutton "Sorrow 3" action mr.Play("audio/bgm/kanasimi2.ogg")
        textbutton "Charge" action mr.Play("audio/bgm/kiki3.ogg")
        textbutton "Field 3" action mr.Play("audio/bgm/field4.ogg")
        textbutton "Deliverance" action mr.Play("audio/bgm/epilogue2.ogg")
        textbutton "Counterattack" action mr.Play("audio/bgm/battle2.ogg")
        textbutton "Monster Lord's Castle 2" action mr.Play("audio/bgm/maouzyou2.ogg")
        textbutton "Sabasa Suppression" action mr.Play("audio/bgm/sabasa2.ogg")
        textbutton "Drain Lab" action mr.Play("audio/bgm/labo2.ogg")
        textbutton "Biolabs" action mr.Play("audio/bgm/labo3.ogg")
        textbutton "World's Navel" action mr.Play("audio/bgm/heso.ogg")
        textbutton "Barrier Tower" action mr.Play("audio/bgm/tower.ogg")
        textbutton "Heaven" action mr.Play("audio/bgm/tenkai.ogg")
        textbutton "Eden" action mr.Play("audio/bgm/eden.ogg")
        textbutton "Promestein 2" action mr.Play("audio/bgm/stein2.ogg")
        textbutton "Black Alice 2" action mr.Play("audio/bgm/maou3.ogg")
        textbutton "Black Alice 3" action mr.Play("audio/bgm/maou4.ogg")
        textbutton "Ilias" action mr.Play("audio/bgm/irias2.ogg")
        textbutton "Final Ilias" action mr.Play("audio/bgm/irias3.ogg")
        textbutton "Awakening" action mr.Play("audio/bgm/epilogue3.ogg")
        textbutton "Credit Roll" action mr.Play("audio/bgm/ending2.ogg")

    frame:
        style_group "vol"
        has vbox

        label "Громкость музыки"
        bar value Preference("music volume") style "pref_scrollbar"
    # Buttons that let us advance tracks.
    frame:
        background Solid("#330000")
        align (.5, 1.0)

        has hbox:
            spacing 20

        textbutton _("NG+ Музыка"):
            text_align 0.0
            text_size 22
            action ShowMenu("music_room_ng")

        textbutton _("<< Назад"):
            text_size 22
            action mr.Previous()

        textbutton _("Вперёд >>"):
            text_size 22
            action mr.Next()

    # Start the music playing on entry to the music room.
    on "replace" action mr.Play()

    use title(_("Музыкальная комната"))
    use return_but("ex")

screen music_room_ng():
    $ ROWS = 50

    tag menu
    add "bg 140"
    add "gm_bg"

    frame:
        style_group "mr"

        has vpgrid:
            rows ROWS

            draggable True
            mousewheel True
            scrollbars "vertical"

            # Since we have scrollbars, we have to position the side, rather
            # than the vpgrid proper.
            # side_xalign 0.5
            # side_yalign 0.3

        # The buttons that play each track.
        textbutton "Главная тема" action mr.Play("NGDATA/bgm/gn_night.ogg")
        textbutton "Монстропедия" action mr.Play("NGDATA/bgm/zukan.ogg")
        textbutton "Смертельная битва" action mr.Play("NGDATA/bgm/granberia.ogg")
        textbutton "Спарринг" action mr.Play("NGDATA/bgm/granberia2.ogg")
        textbutton "Девятихвостый предок" action mr.Play("NGDATA/bgm/tamamo.ogg")
        textbutton "Суккуб ветра" action mr.Play("NGDATA/bgm/alma.ogg")
        textbutton "Королева слизей" action mr.Play("NGDATA/bgm/erubetie.ogg")
        textbutton "Босс 1" action mr.Play("NGDATA/bgm/boss.ogg")
        textbutton "Босс 2" action mr.Play("NGDATA/bgm/boss0.ogg")
        textbutton "Химера" action mr.Play("NGDATA/bgm/boss2.ogg")
        textbutton "Босс 4" action mr.Play("NGDATA/bgm/boss3.ogg")
        textbutton "Босс 5" action mr.Play("NGDATA/bgm/boss4.ogg")
        textbutton "Сёстры Лилит" action mr.Play("NGDATA/bgm/boss5.ogg")
        textbutton "Хаос" action mr.Play("NGDATA/bgm/boss_adra.ogg")
        textbutton "Второй круг" action mr.Play("NGDATA/bgm/field5.ogg")
        textbutton "Юг" action mr.Play("NGDATA/bgm/field6.ogg")
        textbutton "Безмятежность" action mr.Play("NGDATA/bgm/field7.ogg")
        textbutton "Запад" action mr.Play("NGDATA/bgm/field8.ogg")
        textbutton "Восток" action mr.Play("NGDATA/bgm/field9.ogg")
        textbutton "Север" action mr.Play("NGDATA/bgm/field10.ogg")
        textbutton "Гарпия" action mr.Play("NGDATA/bgm/comi2.ogg")
        textbutton "Колизей" action mr.Play("NGDATA/bgm/colosseum.ogg")
        textbutton "Эпилог" action mr.Play("NGDATA/bgm/ruka2.ogg")
        textbutton "Эрозия" action mr.Play("NGDATA/bgm/ruka3.ogg")
        textbutton "Лука" action mr.Play("NGDATA/bgm/ruka4.ogg")
        textbutton "Битва с Духами" action mr.Play("NGDATA/bgm/spirits.ogg")
        textbutton "Духи Стихий" action mr.Play("NGDATA/bgm/spirits2.ogg")
        textbutton "Городок у моря" action mr.Play("NGDATA/bgm/porttown.ogg")
        textbutton "Подводное царство" action mr.Play("NGDATA/bgm/umi3.ogg")
        textbutton "Страх" action mr.Play("NGDATA/bgm/shirome.ogg")
        textbutton "Пирамида" action mr.Play("NGDATA/bgm/pyramid.ogg")
        textbutton "Сфинкс" action mr.Play("NGDATA/bgm/hutuu5.ogg")
        textbutton "Деревня суккубов" action mr.Play("NGDATA/bgm/succubusvillage.ogg")
        textbutton "Вулкан" action mr.Play("NGDATA/bgm/volcano.ogg")
        textbutton "Битва в Вулкане" action mr.Play("NGDATA/bgm/volcano2.ogg")
        textbutton "Болезненные воспоминания" action mr.Play("NGDATA/bgm/hutuu6.ogg")
        textbutton "Таинственность" action mr.Play("NGDATA/bgm/mystery1.ogg")
        textbutton "Первородные сёстры" action mr.Play("NGDATA/bgm/sisters1.ogg")
        textbutton "Алифиса Восьмая" action mr.Play("NGDATA/bgm/alice8th1.ogg")
        textbutton "Илиас" action mr.Play("NGDATA/bgm/irias4.ogg")
        textbutton "Финальная битва" action mr.Play("NGDATA/bgm/iliasbattle.ogg")
    
    frame:
        style_group "vol"
        has vbox

        label "Громкость музыки"
        bar value Preference("music volume") style "pref_scrollbar"
    # Buttons that let us advance tracks.
    frame:
        background Solid("#330000")
        align (.5, 1.0)

        has hbox:
            spacing 20

        textbutton _("Музыка"):
            text_align 0.0
            text_size 22
            action ShowMenu("music_room")

        textbutton _("<< Назад"):
            text_size 22
            action mr.Previous()

        textbutton _("Вперёд >>"):
            text_size 22
            action mr.Next()

    # Start the music playing on entry to the music room.
    on "replace" action mr.Play()

    use title(_("Музыкальная комната"))
    use return_but("ex")

style vol_frame:
    is default
    align (.5, 0.9)
    margin (30, 20)
    padding (10, 10)
    background Frame("images/System/win_bg_0.webp", 5, 5)

style mr_frame:
    is default
    align (.5, 0.3)
    margin (30, 120)
    padding (10, 10)
    background Frame("images/System/win_bg_0.webp", 5, 5)

style mr_vpgrid:
    is default
    xfill True
    spacing 5
    align (.5, .5)

style mr_button:
    is default
    xalign .5
    background None

style mr_button_text:
    is default
    font "fonts/archiform.otf"
    outlines [(2, "#001", 0, 0)]
    size 25
    idle_color "#fff"
    selected_color "#f00"
    hover_color "#00f"
    insensitive_color "#8886"
    insensitive_outlines [(2, "#0008", 0, 0)]

style mr_button_text:
    variant "touch"
    size 28

style mr_vscrollbar:
    is pref_vscrollbar
