label centa_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Well now, you showed up here awfully quick.{w}\nRaped by a horse in a forest... How pathetic."

    show ilias st01 with dissolve #700

    i "Moving on...{w} Being held down by a Centaur is no laughing matter.{w}\nWith her heavy body weight, you will never be able to get her off."
    i "Essentially... Once you're held down, under normal circumstances, your defeat would be assured."
    i "However, with Gnome, you should be able to be imbued with enough power to break free."
    i "Forced to use the power of Elemental Spirits is pathetic for a Hero...{w}\nBut being raped in a forest by a horse is even more so."
    i "As one further point, be careful about using your SP for recovery.{w}\nPowerful sword skills may be the way to go."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDefeat that monster before she attacks an innocent..."

    return


label kaeru_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "You got licked by a bunch of frogs.{w} I'm so happy for you.{w}\nReally, what a pathetic Hero..."

    show ilias st01 with dissolve #700

    i "Now then, if you're captured by the Frog Girls, you'll suffer very damaging attacks."
    i "You can escape normally, but her bind will severely hurt you first."
    i "But with Gnome, you can escape from them much quicker."
    i "In the future, when it looks like you may be overpowered and held with no escape, you may want to borrow Gnome's power."
    i "If you break through all their binds, and still lose... There may be another pathetic fate for you to succumb to."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nPeel off that swarm of Frog Girls, and trample them underfoot."

    return


label alraune_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This time, sucked dry by a flower...{w}\nReally, you are quite pitiful..."

    show ilias st01 with dissolve #700

    i "Alraune uses the ecstasy status effect to leave you helpless while she abuses you.{w}\nIf you use Sylph, you will be able to nullify that move, and any plant-based moves she may have."
    i "If you use Gnome, you'll be able to break out of any restraints... But Sylph may be a better bet."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake Alraune's next blooming be a shower of blood."

    return


label dullahan_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Raped in front of a huge crowd?{w} That disqualifies you as a Hero."
    i "How did it feel to be humiliated in front of a huge crowd of people?"

    show ilias st01 with dissolve #700

    i "The Dullahan is able to attack twice every round.{w}\nShe can also put you into a trance, making her quite troublesome."
    i "Sylph will protect you against the trance, but won't do much against her other moves.{w}\nOn the other hand, Gnome will let you break out of her binds right away."
    i "Either one will help, but I think trance is more troublesome."
    i "Please use those elemental spirits like slaves, then toss them away like useless rags."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nShow her a death even worse than decapitation."

    return


label cerberus_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Preyed on by a dog, does that make you even lower than a pet?{w}\nA Hero whom is even lower than a dog is useless..."

    show ilias st01 with dissolve #700

    i "The Cerberus has powerful attacks, but her restraint is the most worrisome.{w}\nIf you can't get her off in one turn, she will rape you."
    i "Gnome's power will let you escape, so that is the way to go.{w}\nIn addition, there are two ways to lose to her..."
    i "But surely you wouldn't lower yourself again in trying to experience both..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSend that hell hound back to hell."

    return


label alma_elma2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "It seems as though you've suffered an embarrassing defeat against Alma Elma...{w}\nIndeed, having to fight such a powerful opponent is bad..."
    i "But to be defeated when she isn't even trying from the start...{w} Shame on you."

    show ilias st01 with dissolve #700

    i "In fact, she almost seems like she's intrigued by you...{w}\nI think she actually wants to help you..."

    show ilias st02 with dissolve #700

    i "...Such as how to use the wind."

    play sound "audio/se/ikazuti.ogg"
    with Quake((0, 0, 0, 0), 1.5, dist=30)
    show ilias st01 with dissolve #700

    i "...But I will say nothing nice about her."
    i "At any rate, it doesn't seem like you can win fair and square.{w}\nPerhaps this is a fight that can be won with words?"
    i "Usually she would not stop to talk to her prey...{w}\nBut it looks like she makes a special case for you."
    i "It also seems like she has two different ways to humiliate you should you begin to bore her..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOne day for sure, you will destroy that Succubus Queen with your own hands."

    return


label yukionna_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Made into the prey of Yuki-onna?{w} That's just cold...{w}\nBut the thought of me depending on a Hero like you is even more chilling..."

    show ilias st01 with dissolve #700

    i "She has a variety of skills, which make this fight annoying.{w}\nYuki-onna has the ability to both paralyze and drain your HP."
    i "With Gnome you can escape from her embrace easy enough...{w}\nBut with Sylph, you can protect yourself against the paralysis."
    i "Sylph may be the better bet.{w}\nIn either case, at least summon one of them."
    i "In addition, there are multiple endings she can put you through...{w}\nI know you'll be trying them both anyway."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDo not allow yourself to be defeated by the cold."

    return


label nekomata_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Beaten by a bizarre little cat... How pitiful.{w}\nDo you really get that much enjoyment out of betraying my expectations?"

    show ilias st01 with dissolve #700

    i "If you're bound by the Nekomata, you'll be defeated in an instant.{w}\nIf you try to struggle normally, you won't escape."
    i "You must always be using Gnome's power, or you may lose at any time."
    i "Sylph may prevent some damage...{w}\nBut Gnome is the only way to prevent the surefire loss."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nErase any existence of that bizarre beast."

    return


label waelf_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Did you enjoy the Kimono wearing elves' techniques?{w}\nAre you having fun in your quest to be played with by every monster skill?"

    show ilias st01 with dissolve #700

    i "In this battle, you have to fight two enemies at once.{w}\nWhen they're together, their power is quite high."
    i "If you're bound, you will take serious damage.{w}\nWith Gnome, you can break out at once."
    i "In the first half of the fight, Gnome may be safer."
    i "When it's just the Kunoichi Elf alone, her attacks are weaker.{w}\nBut she makes up with it for a devastating pin attack."
    i "If she managed to pin you, she will defeat you without even allowing you to struggle once.{w}\nBecause of that, Gnome is completely useless."
    i "However, Sylph can prevent you from even being bound in the first place.{w}\nSince it's a complicated pin, the protection from the wind will let you avoid it quite easily."
    i "So when you reach the point where it's just the Kunoichi Elf, you should switch to Sylph."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nFinish off those annoying elven creatures."

    return


label yamatanooroti_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Not only did you not get rid of her, but you ended up as the sacrifice...{w}\n...Are you sure you didn't go into that trying to be the sacrifice?"

    show ilias st01 with dissolve #700

    i "Now then, Yamata is quite difficult.{w}\nUnder normal circumstances, victory would be impossible."
    i "All you need to do is reduce her health a little...{w}\nBut even then, that will be difficult."
    i "The most important thing is to escape from her restraint, so Gnome should always be summoned."
    i "After that, you should focus on recovery.{w}\nIt's a long battle, so make sure not to get overwhelmed."
    i "Take care while recovering, and don't let the fight drag on too long.{w}\nIn addition, that powerful blow that attacks at random won't work..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that evil being that torments the villagers."

    return


label insects_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Sucked dry by a bunch of insects...{w}\nYou're really trying to give all your semen to monsters, aren't you?"

    show ilias st01 with dissolve #700

    i "Because of the intense damage from the group of them, you should use Gnome.{w}\nBoth the damage reduction and the ability to break out of binds will be crucial."
    i "Since your opponents are insects, Sylph won't be very useful.{w}\nThis time, stick with Gnome."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThough it's interesting to see monsters kill each other, go ahead and kill them yourself."

    return


label mukade_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Allowing a Centipede Girl to suck up all your body juices... Sheesh.{w}\nAre your body juices really that delicious?"

    show ilias st01 with dissolve #700

    i "The Centipede Girl has such a high attack power, it's almost mandatory to summon Gnome.{w}\nIn addition, her restraint needs to be broken quickly."
    i "If you summon Gnome and fight normally, it shouldn't be too hard.{w}\nShe has a lot of defense though, so it will be a long fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCrush that disgusting Centipede Girl under your foot."

    return


label kaiko_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Did it feel good inside a Silkworm Girl's cocoon?{w}\nDid it feel good to fill that cocoon with your semen?{w}\n...Sheesh."

    show ilias st01 with dissolve #700

    i "The Silkworm Girl has both powerful attacks and an annoying cocoon attack.{w}\nIf you're turned into a cocoon, you'll be met with an instant loss."
    i "If you call Sylph, you may avoid it altogether, but it isn't guaranteed.{w}\nWith Gnome you'll take more damage, but can avoid a forced loss."
    i "Gnome is the safest bet.{w}\nJust be sure to watch your HP."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you become a cocoon again and return here, I'll punish you."

    return


label suzumebati_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "A sex slave of simple worker bees... How pathetic.{w}\nThe fact you accepted it with glee is even worse."

    show ilias st01 with dissolve #700

    i "The Hornet Girl is powerful, and can attack twice.{w}\nIn addition her paralysis is deadly."
    i "With Gnome, you can mitigate some damage.{w}\nBut Sylph will let you dodge the paralysis, which might be better."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you win this battle, you will meet with the insect boss."

    return


label queenbee_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Mating with the Queen, and offering up your semen so happily...{w}\nLuka, your corruption prone behavior is too sickening for words."

    show ilias st01 with dissolve #700

    i "Queen Bee is similar to the Hornet Girl, only with higher attack power.{w}\nShe also has a pesky seduction ability."
    i "Normal attacks can be handled by Gnome, but Sylph will provide protection against the seduction attack."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you eliminate the Queen, the simple minded insects will be thrown into disorder."

    return


label plants1_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This time, losing to a couple of plants...{w}\nSheesh, you have no taste in partners."

    show ilias st01 with dissolve #700

    i "These monsters are not too powerful alone.{w}\nBut together, they are quite troublesome."
    i "But with that said, since they are plants, Sylph is quite effective.{w}\nIf you summon her, you will be able to avoid many of their attacks."
    i "Gnome has been quite useful until now, but Sylph shines against plant types."
    i "It may also be a good idea to quickly unleash powerful sword skills to bring the fight down to a more manageable one versus one."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThis time bring your blade of justice down on the Alraunes."

    return


label plants2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Sheesh, fertilizer for plants now?{w}\nYou really love using your semen to feed monsters, don't you?"

    show ilias st01 with dissolve #700

    i "The three Alraunes are very annoying.{w}\nWithout the power of the spirits, this battle may be completely impossible."
    i "Sylph is the best choice to use.{w}\nSince they're plants, you have a high avoidance chance."
    i "In addition, you must always \"Guard\" to avoid the Parasol's embrace.{w}\nIf you don't, you'll be defeated the next turn."
    i "In addition, once you get down to the final plant, you need to switch to Gnome to break out of her binding attack."
    i "There are many things to avoid, but if you take care, you will win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nMake your sword rip through those plants like a scythe through wheat."

    return


label a_emp_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Falling in love with a flower, and offering it semen of your own free will?{w}\nMy, my.{w} Aren't you just a lovely human being?"

    show ilias st01 with dissolve #700

    i "The plant monster leader is quite powerful herself.{w}\nAs expected of a plant monster, she has both restraining and trance inducing abilities."
    i "The \"Intoxicating Fragrance\" ability is quite dangerous.{w}\nIt lasts for a long time, so taking it may just finish you off."
    i "Sylph will protect you from that, so she is crucial in this fight."
    i "In addition, if that flower clasps around you, your defeat is certain.{w}\nNeither Gnome nor Sylph will be able to stop your loss."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThere are still more enemies to deal with, so don't let your guard down..."

    return


label dorothy_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Food for a plant...{w}\nDo you want to give up your humanity to become an insect?"

    show ilias st01 with dissolve #700

    i "Dorothy's most annoying attack is \"Philia Melt\".{w}\nIf you take this, you will instantly lose."
    i "But Sylph will let you avoid this attack and others, so be sure to use her."
    i "Gnome will help you break from other binds...{w}\nBut you'll be helpless against Philia Melt, so use Sylph."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThey're called Sisters, so be prepared for the enemies ahead..."

    return


label rafi_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "You dissolved in her pitcher?{w}\nGood for you."

    show ilias st01 with dissolve #700

    i "Raffia's pitcher is very dangerous.{w}\nAs soon as you're in there, there's no escape."
    i "The only way to protect yourself from her trance inducing ability is to use Sylph.{w}\nWith Sylph, you should be able to achieve victory."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEven after you defeat this enemy, don't let your guard down."

    return


label dina_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "My, who would have seen this coming?{w}\nDo you want me to keep reincarnating you as a bug so you can let plants eat you endlessly?"

    show ilias st01 with dissolve #700

    i "Like the other sisters, Deana has an annoying trance move that will be your death if it hits you."
    i "If you don't want to be eaten, summon Sylph right away."
    i "Without Sylph, your defeat is almost guaranteed.{w}\nJust watch your health as you whittle hers down."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nWith your sword, obliterate the last of that horrible village of monsters."

    return


label jelly_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Were you able to enjoy the feeling of being submerged in slime?{w}\nI'm glad to see a glimpse of your true self again."

    show ilias st01 with dissolve #700

    i "The Jelly Girl is a slime monster, so Gnome is effective.{w}\nWith Gnome, you can break out of binds and reduce damage."
    i "Slime's use binds and physical moves most often, so Sylph is not very effective."
    i "If you call Gnome, this battle shouldn't be particularly tough."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you come back here again, I'll drown you in slime."

    return


label blob_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This time you were actually eaten by a slime...{w}\nIt seems you enjoy being treated as nothing but a food supply."

    show ilias st01 with dissolve #700

    i "Gnome is required for this fight.{w}\nIf you're captured in her sticky body, your normal strength is not good enough to break free."
    i "Since it's an instant loss if she grabs you without Gnome, you must make sure Gnome is always summoned."
    i "Like the other slime, Sylph's evasion chance is too low for her to be worthwhile."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEradicate all those slimes that would eat humans."

    return


label slime_green_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This time you drowned in the slime...{w}\nRaped by a slime, eaten by a slime, drowned by a slime...{w} Are you satisfied yet?"

    show ilias st01 with dissolve #700

    i "The Green Slime's slime fills up the area, so there's no point struggling."
    i "Just attack her.{w} Once you deal enough damage, the way forward will be shown to you."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThis fight is only the first half..."

    return


label slimes_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This time you were raped by a group of slimes...{w}\nFor a slime maniac like you, that must have been paradise."

    show ilias st01 with dissolve #700

    i "Four slimes at once are as annoying as you would expect...{w}\nThey deal a lot of damage, so don't let Gnome's power go away."
    i "In addition, Gnome can deal with the binds.{w} She's essential.{w}\nSylph? Oh, I almost forgot she existed."
    i "They deal a lot of damage, so a long fight isn't good.{w}\nUse some bold moves right from the start, and grasp victory."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that group of slimes that have made their nest in this dungeon."

    return


label erubetie1_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "A pathetic defeat at the hand of one of the Four Heavenly Knights?{w}\nEven though she's far stronger than you, it's still quite pitiful to watch you be eaten."

    show ilias st01 with dissolve #700

    i "You still can't defeat one of the Heavenly Knights.{w}\nYou should know that better than anyone."
    i "Try to survive a single blow, and a path will open up.{w}\nUse every means possible that you know of to reduce damage."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOne day you'll have the power to defeat her..."

    return


label undine_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "You escaped Erubetie... And ended up being melted away anyway.{w}\nThe digestion lovers thank you for your hard work."

    show ilias st01 with dissolve #700

    i "Undine is another opponent you can't fight on equal terms.{w}\nIf you try to fight normally, you'll be overwhelmed sooner or later."
    i "But Undine seems to want something from you.{w}\nPerhaps if you listen to what she says, you might see the path."
    i "But you should summon Gnome the very first turn to protect yourself from damage, and to break out of a pesky bind attack."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOnce you get tired of being eaten, please try to remember your quest."

    return


label kamakiri_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Mating with a Mantis Girl so she can reproduce...{w}\nWhat sort of Hero are you if you're creating monsters rather than destroying them?"

    show ilias st01 with dissolve #700

    i "Due to her high offensive ability and binding move, Gnome is basically required."
    i "Sylph won't let you evade much, so refrain from using her."
    i "In addition, Undine isn't useful yet.{w}\nShe reduces less damage than Gnome, so refrain from using her at all."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that disgusting monster that tries to mate with humans."

    return


label scylla_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Were you happy being coiled in tentacles?{w}\nYou're quite the perverted Hero to enjoy such humiliation."

    show ilias st01 with dissolve #700

    i "Scylla's restriction attack, as you might expect, is very powerful.{w}\nIf she catches you, she can attack twice every turn."
    i "Gnome is essential in breaking out of it."
    i "In addition, she has a low chance of using a special move while you're bound, causing your defeat to be ensured."
    i "You can use Sylph to protect yourself from it...{w}\nIf you wish to use the SP, you can summon both Sylph and Gnome."
    i "You can summon both to be safe...{w}\nOr you can risk it with just Gnome."
    i "Do whichever you please to win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCut off every one of those disgusting tentacles."

    return


label medusa_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Raped by a bunch of snakes, you met another unsightly end.{w}\nI agree with her, you are quite pitiful."

    show ilias st01 with dissolve #700

    i "Her damage output is high, so Gnome may be useful for that.{w}\nBut she has no binding attack, so Gnome isn't required."
    i "The most troublesome ability she has is petrification.{w}\nIf you can't beat her in three turns after being hit by that attack, you will lose."
    i "Sylph will let you avoid that move, so she is essential.{w}\nIn addition, with Sylph you can power up the new skill you just learned."
    i "It's up to you to decide what to do.{w}\nYou could leave it up to luck and go all out, too."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nCut off her scalp and deprive that monster of her unique trait."

    return


label golem_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Used as a power supply for a synthetic monster...{w} That's a new one.{w}\nSo you've been a toy,{w} eaten,{w} sex slave,{w} sucked dry,{w} a breeding slave,{w} a power source...{w}\nI must say, I'm quite surprised by the variety you enjoy."

    show ilias st01 with dissolve #700

    i "The Golem Girl is very powerful, but there are many turns where she doesn't attack."
    i "Gnome will let you break from her bind, so she should be summoned."
    i "In addition, your defeat is certain if she uses you as her \"power supply\".{w}\nSylph can prevent this, so she should be summoned."
    i "It takes a lot of SP, but it would be safest to summon both of them.{w}\nWith Sylph active, you can also power up your new sword skill."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't bother meeting with humans who would create monsters."

    return


label madgolem_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Squeezed by a man made mud doll this time...{w}\nYou're quite... Liberal in your selection of partners, aren't you?"

    show ilias st01 with dissolve #700

    i "You might want to call Gnome because of her restriction attack.{w}\nHer damage is fairly high, too."
    i "If you're bound when she uses her \"Mud Shake\" move, you will face certain defeat.{w}\nIdeally you would break free with Gnome before she can use it."
    i "Sylph is not very useful in this battle."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn that mud monster back into normal mud."

    return


label artm_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "My, my, you sure seemed to enjoy that sex toy.{w}\nWould you like to continue using that toy in your next life, too?"

    show ilias st01 with dissolve #700

    i "Her restriction attack and damage are both annoying, so Gnome is quite useful."
    i "You can avoid her ecstasy attack with Sylph as well.{w}\nIn addition, you can avoid her restriction attack while bound if you have Sylph summoned."
    i "If you want to be safe, summon both.{w}\nSticking just with Sylph and using your new sword technique should work fine, though."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy that doll, whose existence itself is a sin."

    return


label ant_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "A slave to worker ants, who are already basically slaves of the Queen Ant.{w} That's got to be the lowest of the low.{w}\nWould you like to be reincarnated as a toilet in your next life, by any chance?"

    show ilias st01 with dissolve #700

    i "The Ant Girls deal a lot of damage as a group, so Gnome would be a wise choice."
    i "You can also shake off their restriction, so she's doubly useful."
    i "Sylph is not very effective for this fight...{w}\nThe best plan would be to call Gnome and use sword skills."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEradicate these violent monsters who dared to turn their fangs toward the gentle citizens."

    return


label queenant_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Reproducing with the Ant Queen to create thousands more monsters...{w}\nJust to make sure...{w} You really aren't trying to destroy humanity, right?"

    show ilias st01 with dissolve #700

    i "The Queen Ant's most dangerous move is her \"Eyes of Seduction\".{w}\nSylph will protect you, so be sure to use her."
    i "Outside of that, she has no status or restriction inducing skills."
    i "Gnome is not a requirement for the fight...{w}\nBut since her attacks are pretty damaging, she could be useful."
    i "Using Sylph and your Lightning Sword Flash is a good way to defeat her, though."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy this ringleader of evil and bring peace back to Grangold!"

    return


label maccubus_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "I knew it.{w} You went to the Succubus Village just to be raped by a Succubus, didn't you?{w}\nI should have expected it from you by now..."

    show ilias st01 with dissolve #700

    i "If she binds you, you will be raped the next turn and lose.{w}\nThe only way out is by using Gnome, so she is required."
    i "Outside of that, there's nothing special about this fight.{w}\nJust watch your HP and fight normally."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDefeat those humans who have turned into Succubi without mercy, and protect the innocent humans."

    return


label minccubus_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Are you happy with being a little Succubus girl's toy?{w}\nThough I guess your penis is quite like a toy."

    show ilias st01 with dissolve #700

    i "Mincubus's offensive ability is quite high.{w}\nIf she gets on top of you, you will lose a lot of HP quickly."
    i "Call Gnome as soon as you can to break out of it quickly.{w}\nUnfortunately, Sylph is not very effective."
    i "In addition, it seems like she might get a little overeager and continue two attacks even after you're defeated..."
    i "But only worthless human beings would actually attempt to experience that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEven though your opponent is a little girl, now that she's a Succubus, don't hold anything back."

    return


label renccubus_hansei:
    show ilias st04 with dissolve #700

    i "Having all of your Heroic power stolen by a Succubus...{w}\nIt was really embarrassing to watch you being drained like that..."

    show ilias st01 with dissolve #700

    i "The special ability of the Lencubus is to use {i}Level Drain{/i} while you're bound."
    i "But as long as you {i}Struggle{/i}, you won't suffer from this technique."
    i "In other words, if you actually try to fight, you cannot lose.{w}\nDon't give in to the desire for pleasure like some people..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the Succubi, and return peace to the village!"

    return


label succubus_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Sheesh, lost to another Succubus...{w}\nAre you a Hero, or are you simply food?"

    show ilias st01 with dissolve #700

    i "This Succubus is an annoying opponent, with multiple binding moves, and a status inflicting attack."
    i "If you can't shake off her restraints right away, you will lose instantly.{w}\nTherefore, Gnome must always be summoned."
    i "In addition, both her trance attack and another instant kill move can be avoided only with Sylph."
    i "It's annoying, but you must fight with both Sylph and Gnome.{w}\nWith Sylph summoned, feel free to abuse her with the upgraded thrust move."
    i "In addition, it seems that the Succubus might get a little eager after beating you with two of her attacks...{w}\n...Originally that would be a warning... But since it's you, I'm sure you're taking it as a suggestion."
    i "Please, don't misunderstand me.{w}\nNo true Hero would voluntarily let a monster play with him just to see what happens."

    show ilias st02 with dissolve #700

    i "Now go, oh Wandering Foodstuff Luka.{w}\nPlease, try to fight seriously for once..."

    return


label witchs_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Do you like becoming a Succubus's food that much?{w}\nDid you come to the Succubus Village to let them devour you, or to seal them?"

    show ilias st01 with dissolve #700

    i "The Witch Succubus has less HP and offensive power than normal Succubi.{w}\nBut her special skills are quite troublesome."
    i "You need Sylph to protect against her Level Drain, seduction, and trance attacks.{w}\nBut it isn't a guaranteed evasion chance... So they can still be troublesome."
    i "In addition, if her Minimum Phantasm skill hits you, your defeat is assured.{w}\nSylph can protect you from that, so be sure to summon her."
    i "In addition, if she lands \"Energy Drain\" on you after you're bound, your defeat is certain.{w}\nThus, Gnome is also required..."
    i "It takes time and SP to summon both... But it's the only way to safely fight."
    i "In addition, if she beats you with either her hat or her breasts, she may continue toying with you...{w}\nI hope that wasn't a glint of expectation I just saw in your eyes."
    i "In addition, she may continue playing with you if defeated by either of her Minimum Phantasm skills.{w}\nFor one who loses by choice to such an attack, an end fitting them awaits..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must never allow the revival of Lilith to come about!"

    return


label lilith_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "My, my.{w} As soon as you saw that Lilith had been revived, you sure jumped at the chance.{w}\nI congratulate you on being the first to be raped by the now revived sisters."

    show ilias st01 with dissolve #700

    i "Lilith and Lilim are quite powerful enemies.{w}\nYou won't be able to endure their powerful offensive abilities without the protection of Gnome."
    i "In addition, Sylph is required to protect yourself from her special attacks.{w}\nTo even stand toe to toe with those sisters, you'll need both spirits protecting you."
    i "Since it's a long fight, your SP will be needed for recovery, so be careful on overusing your sword skills."
    i "If they absorb your SP, they will use it in a powerful followup attack, so be sure to defend yourself..."
    i "The sisters are quite formidable, but you should be able to defeat them as you are now."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must never allow the unsealed Lilith to escape back into the world..."

    return


label madaminsect_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Sucked dry by that disgusting insect...{w}\nBeing dry is bad for your skin, Luka."

    show ilias st01 with dissolve #700

    i "You cannot break free from her bind without Gnome, so summoning her is required."
    i "Sylph won't let you evade many attacks, so it may be better to not summon her.{w}\nOther than summoning Gnome, just fight her normally."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nBring the wrath of the heavens down on those who would attack travelers."

    return


label madamumbrella_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Even being forced to come by disgusting tentacles like that...{w}\nDon't you have any shame? How could you let that happen?"

    show ilias st01 with dissolve #700

    i "Just as it would appear, her binding attack is very strong.{w}\nWithout Gnome, there is no way to break free."
    i "Sylph can help you dodge her most powerful attack with a high degree of chance, but isn't really necessary."
    i "But if you do choose to summon her, make use of the powered up sword skill.{w}\nIf you just stick with Gnome, it should be an easy fight however."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nA village such as this should be burnt to the ground."

    return


label maidscyulla_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Prey to tentacles yet again...{w}\nYou have quite a thing for tentacles, don't you?"

    show ilias st01 with dissolve #700

    i "As expected, Scylla Maid's binding attack is fearsome.{w}\nIf you don't break free, she will surely prey on you."
    i "A normal human cannot ever escape from her binding, so you must have Gnome available at all times to free yourself."
    i "But even with Gnome, it will take a lot of time and energy to escape, so you should always be ready for taking large amounts of damage."
    i "Again, Sylph is not very useful.{w}\nHonestly, she may as well be dead to you for a while."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSome tentacle play is fine, but please, try to fight seriously for once..."

    return


label emily_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Killed by a young girl's tentacles this time?{w}\nIf you love tentacles that much, would you like me to reincarnate you as an octopus?"

    show ilias st01 with dissolve #700

    i "Emily is very tough, and has some powerful binding moves.{w}\nDue to that, Gnome is required again."
    i "Even if you like Sylph, she really isn't that useful here.{w}\nBut if you're struggling against her, you could attempt to depend on the low evasion chance and upgraded sword skill."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must defeat the mother and daughter to stop any more people from becoming victims."

    return


label cassandra_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "..............{w} At the very least, it wasn't tentacles this time.{w}\n...But it doesn't seem like that stopped you from enjoying it."

    show ilias st01 with dissolve #700

    i "Cassandra is too powerful for you at the moment.{w}\nBut if you lower her HP enough, a chance should appear."
    i "Gnome is essential in reducing the damage so you can last longer, in addition to breaking out of her binding attack."
    i "You will not be able to out recover her attacks, so you may want to go all out in attacking with your SP."
    i "Let Gnome be your only defensive use of SP, and the rest go toward powerful attacks."
    i "Especially since if you're bound and lose, a horrifying fate awaits..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nEven if she is stronger than you, never lose hope."

    return


label yougan_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Wh...What?{w} Stuck inside a Lava Girl's womb and giving her semen?{w}\nH...How is that even possible?{w}\nWell, I guess you're slightly special in that regard..."

    show ilias st01 with dissolve #700

    i "Gnome is effective against her, so be sure to summon her.{w}\nIn addition, the new skill is quite powerful, so make use of that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nRemember that magma is dangerous to play with."

    return


label basilisk_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st03 with dissolve #700

    i "Turning the penis into stone...{w} Indeed, you could rape a male endlessly like that.{w}\nIs that why you look so happy?"

    show ilias st01 with dissolve #700

    i "Her most dangerous attack is her petrifying one.{w}\nIf you take that three times, you will instantly lose."
    i "Sylph will let you dodge the attack with a high probability, so you should call her.{w}\nThe Basilisk doesn't use a binding attack, so Gnome isn't required."
    i "Using the powered up sword skills, you should be able to quickly take down her HP.{w}\nAs long as you keep the protection from her petrification in mind, it should be an easy fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nIf you come back here after being turned to stone again, I'll set you up in my garden."

    return


label dragon_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "A victim of a Dragon?{w}\nWell isn't that nice...{w} You treated her to a five star meal, apparently."

    show ilias st01 with dissolve #700

    i "The Dragon Girl is a powerful enemy, with high HP, high defense, and high offensive power.{w}\nHer restriction attack is powerful as well."
    i "The only saving grace is that she has no status inducing skills..."
    i "Her binding attack is fatal, so you should summon Gnome.{w}\nSylph won't give great evasion chances, so you can ignore her for this battle if you wish."
    i "Just keep bringing her HP down as you focus on recovery.{w}\nThe flow of the battle should change once you reduce her by about two thirds of her health..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSince it's you, I have faith you can defeat a legendary Dragon..."

    return


label salamander_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "It seems like disappointing me is a hobby of yours.{w}\nIf you're instead simply testing my patience... It's quickly wearing thin."

    show ilias st01 with dissolve #700

    i "Salamander is a powerful monster, proud of her binding attacks.{w}\nFirst of all, Gnome is required to protect yourself from her powerful holds."
    i "She has a lot of HP, but a path will open before it's all depleted.{w}\nSo focus on attacking over recovery, and try to bring her HP down quickly."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nQuench that flame spirit's flames."

    return


label granberia3_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "How pathetic, dying like that."
    i "...What do you mean you didn't die?{w}\nAfter pulling off an act like that, you're dead to me."

    show ilias st01 with dissolve #700

    i "Use Undine's power right at the start if you wish to challenge Granberia.{w}\nIf you don't do that, I'm sorry to say, but it won't be much of a fight."
    i "From now on, you cannot waste SP on careless actions.{w}\nIf you're out of SP at a critical point, your life will be forfeit..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNow is the time to show her your justice."

    return


label kani2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Having your sexual organs washed by someone else... Are you an infant?{w}\nAre you happy now that your penis is nice and clean?"

    show ilias st01 with dissolve #700

    i "She uses a paralyzing move on the first turn that paralyzes you for a long time."
    i "You will take fatal damage if that hits you, so you must use Sylph to dodge it."
    i "In addition, she has a powerful restriction technique that can finish you off.{w}\nTo protect yourself, be sure to summon Gnome as well."
    i "Her defense is fairly high, but her HP isn't.{w}\nYou could just rush her down with techniques."
    i "In addition, her torture will go on for quite a while with a certain washing move, so be careful not to be defeated by it..."
    i "...But why do I get the feeling like you'll be defeated by it anyway?"

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nSeal that monster, and steam her alive in a pot."

    return


label dagon_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "My, how nice it must be to do nothing but live a life of heresy.{w}\nSurely giving in to pleasure isn't worth becoming a slave, right?{w}\nRight?"

    show ilias st01 with dissolve #700

    i "Her restriction attack is very powerful, so don't neglect summoning Gnome.{w}\nIf you're bound without her, your defeat is certain."
    i "Sylph's evasion rate isn't very high, so you're better off ignoring her.{w}\nAlso, the enemy's evasion rate is high, so you may want to use sword skills."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\n...Certainly there is nobody dumb enough to be tricked by a Dagon, right?"

    return


label poseidones_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "[persistent.count_end] times people have watched you... Sheesh."
    i "{w}\nAh, no, don't be worried about what I mean by that."

    show ilias st01 with dissolve #700

    i "Poseidoness is a very powerful enemy that uses both status attacks and binding moves.{w}\nTaking either of them will leave you hurting."
    i "Having both Sylph and Gnome active will go a long way in reducing damage."
    i "Sylph will let you avoid paralysis, and Gnome will let you break free of her binds.{w}\nThere's an instant kill move with her bind, so Gnome is particularly good..."
    i "It's a long fight, so you may want to shy away from sword skills.{w}\nFocus on recovery, and take her down."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nGive your judgement to that stupid monster posing as some sort of Queen."

    return


label seiren_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "It looks like you really enjoyed that Siren's song.{w}\nHow odd...{w} Since you keep ignoring my scolding, I thought you went deaf."

    show ilias st01 with dissolve #700

    i "The Siren's scariest ability is her song.{w}\nIf her opponent hears that, they'll be doomed."
    i "You can protect yourself from her song with Sylph.{w}\nGnome isn't really needed for this fight."
    i "As long as you protect yourself from her song, there isn't much to be afraid of.{w}\nJust fight normally and you'll win."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nFor the security of all those who travel on the sea, destroy that monster."

    return


label hitode_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Even though you knew you were going to be eaten, you still managed to ejaculate...{w}\nYou really are a pathetic premature ejaculator, aren't you?"

    show ilias st01 with dissolve #700

    i "The Starfish Girl's binding attack is strong.{w}\nYou need Gnome to break free, so make sure you call her."
    i "Sylph won't let you evade many attacks, so you can ignore her.{w}\nIf you just use Gnome and the powered up sword move, this will be an easy fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThis time, let her feast on your blade."

    return


label beelzebub_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st14 with dissolve #700

    i "...............{w}\n..........................................."

    show ilias st06 with dissolve #700

    i ".....................................................{w}\n............"

    show ilias st04 with dissolve #700

    i "*Sigh*"

    show ilias st01 with dissolve #700

    i "Their attacks are powerful and dangerous.{w}\nYou will want both Sylph's evasion and Gnome's reduction to reduce as much damage as possible."
    i "In addition, Gnome is essential due to their powerful binds.{w}\nSylph isn't required, but dodging attacks every so often will help a lot."
    i "The path forward will open if you can just manage to defeat one of them..."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou must not allow those disgusting fly monsters to reach the outside world."

    return


label trickfairy_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "A little Fairy's toy... Pathetic as ever, I see.{w}\nI'm ashamed at myself for having become used to your pathetic behavior."

    show ilias st01 with dissolve #700

    i "The Trick Fairy doesn't have any special skills or binding moves.{w}\nDue to that, you should use your SP on offensive skills."
    i "She's able to pierce through the defenses of some spirits, so Sylph and Gnome aren't that useful."
    i "Perhaps using Undine will be effective."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nPlease stop disappointing me."

    return


label queenfairy_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "The Queen Fairy made you nourish the forest?{w}\nI've been thinking it for a while now, but...{w} To think you really were the same as manure..."

    show ilias st01 with dissolve #700

    i "The Queen Fairy has some difficult skills.{w}\nIt will be hard to fight her head on."
    i "Many of her moves are plant based, so Sylph will go a long way in helping you avoid them."
    i "Gnome will be helpful for her binds and the extra defense.{w}\nSummon both of them to have a more manageable time."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nExterminate all the wicked monsters hiding away on this island."

    return


label queenelf_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st01 with dissolve #700

    i "Oh, how kind of her.{w}\nPerhaps now that you can't make any more semen, you'll finally be able to fight seriously."
    i "The Queen Elf uses a variety of rape attacks.{w}\nShe's a twisted, lustful monster that only thinks about raping men."
    i "If you try to fight without Gnome and Sylph, you will surely be raped."
    i "Be very careful about not losing Gnome and Sylph.{w}\nYou may want to just be careful and be on the defensive most of the fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nShow those monsters that they aren't even welcome on this island."

    return


label saraevil_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Did you enjoy Sara's vagina now that she's a Succubus?{w}\n...Sheesh, what reason did you set out on an adventure for, anyway?{w}\nIs your quest to make sure you're raped by every girl you come across?"

    show ilias st01 with dissolve #700

    i "Sara has two types of restriction moves that will both defeat you right away if you don't break free with Gnome."
    i "She uses a trance ability, but Sylph's evasion isn't guaranteed.{w}\nSara has high offensive power, so any defensive help would be useful, though."
    i "Since Sara deals a lot of damage and you have both spirits active, a long fight may be unavoidable as you spend most of your SP on recovery."
    i "Lastly, Sara may decide to alter your fate a little bit if she defeats you with two certain moves.{w}\nDon't disgrace me by finding out which two, though."
    i "...Heed my warning for once."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nShow no mercy to a monster, even if they used to be human."

    return


label wyvern_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Raped while flying in the air...{w} That's certainly a new one.{w}\nWere you able to enjoy entering that exclusive club?"

    show ilias st01 with dissolve #700

    i "Your enemy is an offshoot of the dragon race.{w}\nAs such, your attacks won't do much damage to her."
    i "But you should be able to use a new power in this fight...{w}\nYou should save your SP until a path opens up."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nLand on that continent, and exterminate all the monsters that live there."

    return


label kyoryuu_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "This is the first time I've had a Hero retire to become a piece of candy.{w}\nYour bizarre interests never cease to amaze me."

    show ilias st01 with dissolve #700

    i "You cannot struggle out of her restriction move.{w}\nThe only way to escape is to attack her from the inside."
    i "Sylph can help you avoid her paralyzing move, so she is helpful.{w}\nIn addition, the Kyoryuu's damage output is huge, so Gnome will be useful."
    i "Fighting carefully with Sylph and Gnome can be good, but trying to go all out with powerful moves may bring victory, too."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThe monsters of Hellgondo may be strong, but you must not lose heart."

    return


label c_beast_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "..............."

    show ilias st01 with dissolve #700

    i "The Chimera Beast uses a skill that will bring your instant loss.{w}\nSylph will let you avoid it, so you must make sure to call her."
    i "Her offensive ability is high, so Gnome may be useful...{w}\nBut she has no binding moves, so you could always go for an all out attack."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nNever forget your original mission."

    return


label c_dryad_vore_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "...Just what did you want to do in Remina, anyway?{w}\nYour destination is the Monster Lord's castle."

    show ilias st01 with dissolve #700

    i "The Chimera Dryad Vore is a plant based monster, so Sylph is quite effective."
    i "Gnome isn't required, but she can be useful in escaping from binds quicker.{w}\nIn addition, her offensive output is fairly high, so the increase in defense will be helpful."
    i "Her different binding moves require different ways to escape.{w}\nYou'll want to struggle as normal to escape from her ivy."
    i "But the venus flytrap won't let you go no matter how hard you struggle, so you'll want to attack it instead."
    i "In addition, you'll want to guard and prepare yourself if she looks to use her pitcher plant.{w}\nIf you don't guard that attack, your fate will be sealed."
    i "In addition, there are three different fates you can be doomed to with this monster...{w}\nThey all lead to you being eaten though, so you should avoid them."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nYou have no business here, leave quickly."

    return


label vampire_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "As soon as you saw that Vampire, you got excited at the thought of her sucking your blood.{w}\nThat's why you're on this journey after all, isn't it?"
    i "If that wasn't right, then you wouldn't have appeared before me like this.{w}\nAhh... Why have you forgotten your mission...?"

    show ilias st01 with dissolve #700

    i "The Vampire is a powerful enemy, with lots of special skills."
    i "Her most dangerous skill is her eyes of obedience...{w}\nWithout Sylph, you'll easily be taken down."
    i "In addition, Gnome will help you escape from her binding move.{w}\nYou'll want to summon both Sylph and Gnome to fight safely."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nExterminate that wicked monster known as a Vampire."

    return


label behemoth_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Oh my, was that really rape?{w}\nIt seemed as though you really enjoy that sort of wild, animalistic sex."

    show ilias st01 with dissolve #700

    i "Her damage output is huge, so Gnome is useful.{w}\nIn addition, you won't escape from her bind without Gnome."
    i "She uses a horrible technique if you're bound...{w}\nBut Sylph will let you dodge it, so you'll want to summon her to be careful."
    i "With Gnome, you might break out of the bind before she uses it...{w}\nBut if you're unlucky..."
    i "Just use those spirits like rags, then throw them away when they're not needed any longer."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDon't lose heart just because strong monsters are appearing one after another."

    return


label esuccubus_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "You march into the Monster Lord's Castle.{w} You see a Succubus.{w} You instantly lose.{w}\nWhy am I not surprised by this any longer?{w}\nJust why did you even bother to come here?"

    show ilias st01 with dissolve #700

    i "A powerful Succubus, she can use various techniques that will instantly defeat you."
    i "Summon Sylph to evade these quick losses.{w}\nIn addition, if you don't break free right away from her bind, you will be defeated by an embarrassing move."
    i "If you don't have Sylph and Gnome both up, you're risking an instant death at any point."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nOnly powerful enemies await in the Monster Lord's Castle, so never let your guard down."

    return


label hatibi_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Thank you for all your hard work!{w}\nCongratulations, you're now a perverted Kitsune's toy!"

    show ilias st01 with dissolve #700

    i "Despite her dangerous eye of seduction, her binding move can kill you in one turn without Gnome."
    i "You can protect yourself from her eye with Sylph, but she isn't required."
    i "In addition, when she starts to charge her magical power, a strong attack will follow...{w}\nIf you hope to win the fight, you should guard the next turn."
    i "In addition, there's a different ending if you're finished in a bound state...{w}\nKnowing you, I'm sure I'll see you back before me in a few moments after learning about that."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nTurn that Kitsune into Inarizushi."

    return


label inp_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "To an Imp...?{w} A weak Imp?{w} You could have tapped her and she would have lost...{w}\nTo let yourself be seduced so easily... Have you no shame?"

    show ilias st01 with dissolve #700

    i "There is nothing to be careful about in this fight.{w}\nThe only way to lose is if you are a failure as a Hero."
    i "There are two ways to be defeated by her, though.{w}\nBut I wouldn't recommend angering me any further."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nThe wrath of heaven will be brought down upon you soon if you don't get a little serious."

    return


label gigantweapon_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Giganto Weapon...{w}\nThat prototype was still able to move...?"

    show ilias st01 with dissolve #700

    i "Her attack power is extremely high, so you'll need Sylph and Gnome to mitigate as much damage as possible."
    i "In addition, Sylph will help you avoid a status inducing attack.{w}\nThere's no bind, but her attacks will be too painful to endure without Gnome."
    i "If you see her charging up a move, make sure you guard.{w}\nOtherwise your death will be assured the next turn."
    i "Her HP is enormous, so a long battle can't be avoided.{w}\nBut since you were able to make it all the way here, you should be able to beat her."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDispose of that biological weapon."

    return


label alma_elma3_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Have you been left dry by the Queen Succubus?{w}\nYou can't hide your satisfied face from me...{w}\nAnd I'm not even going to try hiding my disappointment."

    show ilias st01 with dissolve #700

    i "Until she takes off her clothes, she isn't fighting seriously.{w}\nYou shouldn't need any help until that point."
    i "Once she gets serious, her attacks become fierce.{w}\nYou must have Sylph summoned at all times to stand a chance."
    i "In addition, you run the risk of an instant loss at anytime without Gnome summoned."
    i "A powered up Sylph will let you attack twice per turn.{w}\nIn addition, Gnome will increase your critical hit chance, granting a nice synergy."
    i "Using that barrage of attacks, make sure you use your SP for recovery.{w}\nIf you waste them trying to shorten the fight, you'll just be shortening your own life instead."
    i "Lastly, the fickle Alma Elma may decide to do something else with you if you lose to a certain attack."
    i "Go ahead and try to find it. I know you'll be doing it no matter what I say."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAs you are now, you should be able to defeat the Four Heavenly Knights."

    return


label tamamo2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Defeated by the Kitsune leader, and content with a life as her lover...?{w}\nYour failures as a Hero continue to pile up right at the end of your quest."

    show ilias st01 with dissolve #700

    i "At the start, Tamamo isn't fighting seriously at all.{w}\nJust summon spirits as the situation calls for."
    i "But once Tamamo takes off her clothes, her attacks become a lot more serious."
    i "Her damage output is extreme, so Gnome is required.{w}\nIn addition, you risk an instant loss if you can't break free of her bind."
    i "She is able to rape you in an instant, but you can avoid it with Sylph.{w}\nIn addition, many of her tail attacks have a high avoidance chance with Sylph."
    i "Fighting with Gnome and Sylph together is the best method.{w}\nIn addition, the double criticals will go a long way."
    i "Lastly, she uses a unique skill to bring you to your end if you lose before she becomes serious.{w}\nGo ahead and shame me by viewing it, since I know you will anyway."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the leader of the Kitsunes, and throw their entire race into confusion."

    return


label erubetie2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st02 with dissolve #700

    i "Now I know you've enjoyed Slimes ever since being a child...{w}\nHow did it feel turning back into a child, to a time before you started to disappoint me?"

    show ilias st04 with dissolve #700

    i "...Oh, silly me.{w} Your first thought was about how you could disappoint me."

    show ilias st01 with dissolve #700

    i "Both her offensive power and ability to bind are quite strong, so Gnome is a must."
    i "Erubetie is a slime, so Sylph isn't very effective...{w}\nBut she will let you dodge one incredibly powerful move when you're in a bind state."
    i "Gnome is essential, and Sylph should be summoned as much as possible."
    i "In addition, her Melt Storm ability will deal up to 800 damage even with Gnome summoned."
    i "If your HP is less than 800, you risk an instant loss."
    i "Next, \"Heaven's Prison\" can't be broken from normally.{w}\nYou'll have to attack to escape."
    i "She's a difficult enemy, with many things to watch out for...{w}\nBut you should be able to beat her as you are now."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nDestroy the Queen of the Slimes, and defile their homeland, too."

    return


label granberia4_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "You came so far, only to end up as Granberia's plaything...{w}\nI see you couldn't help but disappoint me one final time."

    show ilias st01 with dissolve #700

    i "First, show her the power of the four spirits.{w}\nThen you should be able to fix what you are lacking..."
    i "After that, the real battle will start.{w}\nAs you already know, her offensive power is tremendous."
    i "Make full use of the spirits, and you should be able to reach victory after a difficult fight."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nI believe that your sword can overcome Granberia's."

    return


label alice2_hansei:
    $ persistent.count_hanseikai2 += 1
    show ilias st04 with dissolve #700

    i "Did you think it would be funny for a Hero to lose to the Monster Lord?{w}\nThough I cannot say I particularly dislike this ending, since she sealed herself away for eternity..."
    i "I'll give you one more chance.{w}\nDon't fail my expectations."

    show ilias st01 with dissolve #700

    i "Naturally, the Monster Lord is the most powerful monster you've faced.{w}\nIf you don't use all four spirits, you cannot win."
    i "Gnome is required to break free of her bind, and Sylph is required to evade her temptation attack."
    i "If you aren't both guarding and in a serene state of mind when she unleashes a charged attack, evasion will be impossible."
    i "Without Earth, Water and Wind, your defeat is all but guaranteed.{w}\nMake sure you always have enough SP to keep those summons up."
    i "In addition, her magical power is extremely strong.{w}\nWind and Water will raise your evasion rate, but it isn't guaranteed..."
    i "As a precaution, don't let your HP drop below 900.{w}\nYou should try to be fighting at full HP as much as possible."
    i "When you reach the end, she may begin to prattle or something.{w}\nMake sure you give her a decisive blow at that point."
    i "...Do you understand, Luka?{w}\nA True Hero would use their strongest attack to give their finishing blow to the Monster Lord."

    show ilias st02 with dissolve #700

    i "Now go, oh brave Luka.{w}\nAt the time of your victory, I shall offer half the world to you."

    return


