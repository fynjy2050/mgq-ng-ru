image alice st01 = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st01.webp",
    "ribbon == 1", im.Composite((456, 600),
                        (0, 0), "images/Characters/alice/alice st01.webp",
                        (0, 0), "images/Characters/alice/alice st91.webp"))

image alice st01b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st01b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st01b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st02 = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st02.webp",
    "ribbon == 1", im.Composite((456, 600),
                        (0, 0), "images/Characters/alice/alice st02.webp",
                        (0, 0), "images/Characters/alice/alice st91.webp"))

image alice st02b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st02b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st02b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st03 = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st03.webp",
    "ribbon == 1", im.Composite((456, 600),
                        (0, 0), "images/Characters/alice/alice st03.webp",
                        (0, 0), "images/Characters/alice/alice st91.webp"))

image alice st03b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st03b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st03b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st04 = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st04.webp",
    "ribbon == 1", im.Composite((456, 600),
                        (0, 0), "images/Characters/alice/alice st04.webp",
                        (0, 0), "images/Characters/alice/alice st91.webp"))

image alice st04b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st04b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st04b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st05b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st05b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st05b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st07b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st07b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st07b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st08b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st08b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st08b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st09b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st09b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st09b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

# image alice st11b = ConditionSwitch(
#     "ribbon == 0", "images/Characters/alice/alice st11b.webp",
#     "ribbon == 1", im.Composite((666, 600),
#                         (0, 0), "images/Characters/alice/alice st11b.webp",
#                         (0, 0), "images/Characters/alice/alice st91b.webp"))

image alice st51b = ConditionSwitch(
    "ribbon == 0", "images/Characters/alice/alice st51b.webp",
    "ribbon == 1", im.Composite((666, 600),
                        (0, 0), "images/Characters/alice/alice st51b.webp",
                        (0, 0), "images/Characters/alice/alice st91b.webp"))

# персонаж в главном меню
image gm char = ConditionSwitch(
    "(player == 0 or gm_char == 0) and store.skillset == 1",
        Image("images/Characters/luka/luka st02.webp", align=(0, 0), pos=(130 ,25)),
    "(player == 0 or gm_char == 0) and store.skillset != 1",
        Image("images/Characters/luka/luka st03.webp", align=(0, 0), pos=(20 ,25)),
    "(player == 1 or gm_char == 1)",
        Image("images/Characters/alice/alice st01.webp", align=(0, 0), pos=(250 ,25)),
    "(player == 2 or gm_char == 2)",
        Image("images/Characters/alma_elma/alma_elma st21.webp", align=(0, 0), pos=(100 ,0)),
    "(player == 3 or gm_char == 3)",
        Image("images/Characters/erubetie/erubetie st01.webp", align=(0, 0), pos=(325 ,0)),
    "(player == 4 or gm_char == 4)",
        Image("images/Characters/tamamo/tamamo st04.webp", align=(0, 0), pos=(100 ,0)),
    "(player == 5 or gm_char == 5)",
        Image("images/Characters/granberia/granberia st01.webp", align=(0, 0), pos=(100 ,0)),
    "(player == 6 or gm_char == 6)",
        Image("images/Characters/hapy_a/hapy_a st11.webp", align=(0, 0), pos=(100 ,0)),
    "(player == 7 or gm_char == 7)",
        Image("images/Characters/youko/youko st02.webp", align=(0, 0), pos=(100 ,0)))


# лого
image logo = ConditionSwitch(
    "persistent.NGMODE == True",
        Image("NGDATA/images/System/logo.webp", align=(.5, .25)),
    "_preferences.language == None and persistent.game_clear < 3",
        Image("images/System/logo.webp", align=(.5, .25)),
    "_preferences.language == 'russian' and persistent.game_clear < 3",
        Image("images/System/logo_rus.webp", align=(.5, .25)),
    "_preferences.language == None and persistent.game_clear == 3",
        Image("images/System/logox.webp", align=(.5, .25)),
    "_preferences.language == 'russian' and persistent.game_clear == 3",
        Image("images/System/logox_rus.webp", align=(.5, .25)))