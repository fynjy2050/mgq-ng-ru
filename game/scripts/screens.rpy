##############################################################################
# Say
#
# Screen that's used to display adv-mode dialogue.
# http://www.renpy.org/doc/html/screen_special.html#say
screen say(who=" ", what="", side_image=None, two_window=False):

    # Show HP window or hide HP window
    if renpy.get_screen("hp"):
        $ SAYW_YOFFSET = - 75
        $ SAYW_YSIZE = 110
    else:
        $ SAYW_YOFFSET = - 3
        $ SAYW_YSIZE = 160
        $ appear_name = ""

    $ LM = 5
    $ RM = 5

    # use elm indicator
    if turn_scorer > 0:
        use turn_scorer

    if wind or aqua or fire or earth:
        $ LM = 25
        use elm

    # use enemy elm indicator
    if enemy_wind or enemy_aqua or enemy_fire or enemy_earth:
        $ RM = 25
        use enemy_elm

    vbox:
        style "say_two_window_vbox"
        yalign 1.0

        hbox:
            yoffset SAYW_YOFFSET
            xfill True
            spacing 0

            window:
                # id "window"
                style "say_who_window"
                background Frame(im.MatrixColor(
                                    persistent.box,
                                    im.matrix.colorize(persistent.box_color, persistent.box_color)
                                    ),
                                Borders(10, 10, 10, 10),
                                tile=True
                                )
                xminimum 250
                margin (5, 5)
                align (0, 1.0)
                padding (5, 5, 5, 0)

                # if who:
                text who:
                    xalign .5
                    id "who"
                    font store.main_font.path
                    size store.main_font.size
                    kerning store.main_font.kerning
                    line_leading store.main_font.leading
                    line_spacing store.main_font.spacing
                    bold store.main_font.bold
                    italic store.main_font.italic
                    color persistent.who_color
                    outlines [(persistent.who_out_thickness, persistent.who_out_color, 0, 0), (2, "#0003", 2, 2)]
                # else:
                #     text " "

            # Use the quick menu.
            use quick_menu

        window:
            id "window"
            background Frame(im.MatrixColor(
                                    persistent.box,
                                    im.matrix.colorize(persistent.box_color, persistent.box_color)
                                    ),
                                Borders(10, 10, 10, 10),
                                tile=True
                                )

            yoffset SAYW_YOFFSET
            ysize SAYW_YSIZE
            left_margin LM
            right_margin RM
            top_margin 0
            xfill True
            padding (10, 10)

            has vbox:
                style "say_vbox"

            text what:
                id "what"
                adjust_spacing False
                font store.main_font.path
                size store.main_font.size
                kerning store.main_font.kerning
                line_leading store.main_font.leading
                line_spacing store.main_font.spacing
                bold store.main_font.bold
                italic store.main_font.italic
                color persistent.what_color
                outlines [(persistent.what_out_thickness, persistent.what_out_color, 0, 0), (2, "#0003", 2, 2)]

    if addon_on == 1 or saveback == 0:
        key "rollback" action ShowMenu("history", enter="spinoff")

init python:
    style.default.line_overlap_split = -2

##############################################################################
# Choice
#
# Screen that's used to display in-game menus.
# http://www.renpy.org/doc/html/screen_special.html#choice

screen choice(items):

    window:
        style "menu_window"
        align (.5, .5)

        vbox:
            style "menu"
            spacing 2

            for caption, action, chosen in items:

                if action:

                    button:
                        background Frame(im.MatrixColor(persistent.box,
                                            im.matrix.colorize(persistent.box_color, persistent.box_color)),
                                            Borders(15, 15, 15, 15),
                                            tile=True)
                        xalign .5
                        action [action, If(hp_scr == 1, Show("hp"))]
                        style "menu_choice_button"

                        text caption:
                            style "menu_choice"
                            font persistent.what_font
                            color persistent.what_color
                            hover_size persistent.what_font_size + 3
                            idle_size persistent.what_font_size + 2
                            outlines [(persistent.what_out_thickness, persistent.what_out_color, 0, 0),
                              (persistent.what_out_thickness, "#0003", 2, 2)]

                    #null height config.screen_height * 0.05

                else:
                    text caption style "menu_caption"

    if renpy.variant("touch"):
        hbox:
            align (.5, 1.0)

            if saveback > 0:
                imagebutton:
                    # align (1.0, 0)
                    at xyzoom(.9)
                    auto "back_%s"
                    margin (5, 3)
                    action Rollback()
                imagebutton:
                    # align (1.0, 0)
                    at xyzoom(.9)
                    auto "save_%s"
                    margin (5, 3)
                    action ShowMenu("save", enter="game")

            imagebutton:
                # align (1.0, 0)
                at xyzoom(.9)
                auto "load_%s"
                margin (5, 3)
                action ShowMenu("load", enter="game")
            imagebutton:
                # align (1.0, 0)
                at xyzoom(.9)
                auto "menu_%s"
                margin (5, 3)
                action ShowMenu()


$ config.narrator_menu = True

style menu_window is default

style menu_choice is button_text:
    clear

style menu_choice_button is button:
    idle_xminimum int(config.screen_width * 0.75)
    hover_xminimum int(config.screen_width * 0.8)

    idle_xmaximum int(config.screen_width * 0.75)
    hover_xmaximum int(config.screen_width * 0.8)

    idle_yminimum int(config.screen_height * 0.10)
    hover_yminimum int(config.screen_height * 0.11)
    idle_ymaximum int(config.screen_height * 0.10)
    hover_ymaximum int(config.screen_height * 0.11)

style menu_choice_button is button:
    variant "touch"
    idle_yminimum int(config.screen_height * 0.14)
    hover_yminimum int(config.screen_height * 0.15)
    idle_ymaximum int(config.screen_height * 0.14)
    hover_ymaximum int(config.screen_height * 0.15)


##############################################################################
# Main Menu
#
# Screen that's used to display the main menu, when Ren'Py first starts
# http://www.renpy.org/doc/html/screen_special.html#main-menu

screen main_menu(extras=False):

    # on "show" action If(extras, ShowMenu(screen="extras"))
    # on "replace" action If(extras, ShowMenu(screen="extras"))

    # This ensures that any other menu screen is replaced.
    tag menu

    if persistent.show_extras_menu:
        use extras
    else:
        # The background of the main menu.
        add "bg 164"

        add "logo" xalign .5 yalign .35

        # window:
        #     style "mm_root"

        # The main menu buttons.
        frame:
            style_group "mm"

            use choice_box:
                textbutton "Новая игра +" at appear(1.0) action Start()
                textbutton "Продолжить" at appear(1.0) action ShowMenu("load", enter="mm")
                textbutton "Настройки" at appear(1.0) action ShowMenu("prefs", enter="mm")
                textbutton "Экстра" at appear(1.0) action [Play("music", "NGDATA/bgm/zukan.ogg"),
                                                                   SetField(persistent, "show_extras_menu", True)]
                textbutton "Выйти" at appear(1.0) action Quit(confirm=False)

screen choice_box:
    if renpy.variant("touch"):
        hbox:
            transclude
    else:
        vbox:
            transclude

# Make all the main menu buttons be the same size.
style mm_frame:
    background None
    align (.5, .9)

style mm_frame:
    variant "touch"
    align (.5, .75)

style mm_button:
    size_group "mm"
    align (.5, .5)
    idle_background Solid("#330000")
    hover_background Solid('#888')

style mm_button:
    variant "touch"
    xysize (140, 100)

style mm_hbox:
    variant "touch"
    spacing 20

style mm_button_text:
    font "fonts/archiform.otf"
    size 20
    idle_color "#e6e6fa"
    hover_color "#330000"
    idle_outlines [(1, "#330000")]
    hover_outlines [(1, "#888")]


##############################################################################
# Yes/No Prompt
#
# Screen that asks the user a yes or no question.
# http://www.renpy.org/doc/html/screen_special.html#yesno-prompt
screen confirm(message, yes_action, no_action):

    modal True

    style_group "yesno"

    if persistent.game_clear == 3:
        add "bg 140"
    else:
        add "bg 001"

    if message == layout.QUIT:
        $ message = _("Вы уже хотите покинуть Девушек-Монстров?")
        # $ message = "Вы уже хотите покинуть Девушек-Монстров?"
        add "quit" align(.5, .5)

    label _(message):
        xalign 0.5

    hbox:
        align (.5, .8)
        spacing 100

        textbutton "Да" action yes_action
        textbutton "Нет" action no_action

    # Right-click and escape answer "no".
    key "game_menu" action no_action


style yesno_label:
    background Frame('images/System/win_bg_0.webp', 5, 5)
    xmargin 30
    xfill True
    yminimum 100
    align (.5, .1)
    padding (10, 10)


style yesno_label_text:
    text_align 0.5
    layout "subtitle"
    font "fonts/archiform.otf"
    align (.5, .5)
    size 22

style yesno_button:
    background Frame('images/System/win_bg_0.webp', 5, 5)
    xysize (150, 90)

style yesno_button_text:
    font "fonts/archiform.otf"
    align (.5, .5)
    size 22
    color "#fff"

##############################################################################
# Quick Menu
#
# A screen that's included by the default say screen, and adds quick access to
# several useful functions.
screen quick_menu():

    # Add an in-game quick menu.
    hbox:
        style_group "quick"
        spacing 10

        xalign 1.0
        yalign 1.0

        if addon_on or saveback == 0:
            textbutton "Журнал" action ShowMenu("history", enter="spinoff")
        else:
            textbutton "Назад" action Rollback()

        textbutton "Авточтение" action Preference("auto-forward", "toggle")
        textbutton "Пропуск" action Skip()
        textbutton "Сохранить" action If(addon_on == 0 and saveback > 0, ShowMenu("save", enter="g"))
        textbutton "Загрузить" action If(addon_on == 0, ShowMenu("load", enter="g"))

        if addon_on:
            textbutton "В меню" action Jump("spinoff_start")


screen quick_menu():
    variant "touch"
    # Add an in-game quick menu.

    if not renpy.get_screen("qm2"):

        vbox:
            xalign 1.0

            imagebutton:
                # at xyzoom(.9)
                auto "back_%s"
                margin (5, 3)
                action Rollback()
            imagebutton:
                # at xyzoom(.9)
                auto "menu_%s"
                margin (5, 3)
                action Show("qm2")
screen qm2():

    modal True
    predict False

    hbox:
        align (.5, .5)

        imagebutton:
            at xyzoom(1.2)
            yalign 1.0
            idle "auto_idle"
            hover "auto_hover"
            selected_idle "auto_hover"
            selected_hover "auto_hover"
            margin (5, 3)
            action [Hide("qm2"), Preference("auto-forward", "toggle")]
        imagebutton:
            at xyzoom(1.2)
            yalign 1.0
            idle "skip_idle"
            hover "skip_idle"
            selected_idle "skip_hover"
            selected_hover "skip_hover"
            insensitive "skip_insensitive"
            margin (5, 3)
            action [Hide("qm2"), Skip()]

        imagebutton:
            at xyzoom(1.2)
            yalign 1.0
            auto "save_%s"
            margin (5, 3)
            action [Hide("qm2"), Show("aaa", m="save")]
        imagebutton:
            at xyzoom(1.2)
            yalign 1.0
            auto "load_%s"
            margin (5, 3)
            action [Hide("qm2"), ShowMenu("load", enter="game")]

        imagebutton:
            at xyzoom(1.2)
            auto "menu_%s"
            margin (5, 3)
            action [Hide("qm2"), Show("aaa")]

        imagebutton:
            at xyzoom(1.2)
            # at xyzoom(.9)
            auto "back_%s"
            margin (5, 3)
            action Hide("qm2")

screen aaa(m=""):
    if m:
        timer 0.01 action [Hide("aaa"), FileTakeScreenshot(), ShowMenu("save", enter="game")]
    else:
        timer 0.01 action [Hide("aaa"), FileTakeScreenshot(), ShowMenu()]


style quick_button:
    is default
    background None

style quick_button_text:
    is default
    idle_color "#888"
    hover_color "#fff"
    selected_color "#fff"
    outlines [(2, "#000e")]
    insensitive_color "#8888"
    insensitive_outlines [(2, "#000a")]
    size 14
    font "fonts/archiform.otf"


#########################################################################
#
# CITY MENU
#
screen city():
    predict False

    frame:
        style_group 'city'
        background Frame(im.MatrixColor(
                            persistent.box,
                            im.matrix.colorize(persistent.box_color, persistent.box_color)
                            ),
                        15, 15, tile=True
                        )

        grid 2 9:
            xfill True
            transpose True

            for i in xrange(1, 19):
                $ j = ("0%d" % i) if i < 10 else ("%d" % i)

                textbutton "%s" % getattr(store, "city_button%s" % j):
                    action If(getattr(store, "city_button%s" % j) and getattr(store, "city_act%s" % j), Return(i))


    textbutton city_button19:
        background Frame(im.MatrixColor(persistent.box,
                                        im.matrix.colorize(persistent.box_color, persistent.box_color)),
                                        Borders(15, 15, 15, 15),
                                        tile=True)
        action If(city_button19 != '' and city_act19 == 1, Return(19))
        align (.5, .85)
        xminimum 300
        yminimum 50
        text_font "fonts/blogger.ttf"
        text_outlines [(2, "#000", 0, 0)]
        text_size 21
        text_idle_color "#fff"
        text_hover_color "#00f"
        text_insensitive_color "#80808060"
        text_insensitive_outlines [(2, "#00000080", 0, 0)]

    if renpy.variant("touch"):
        hbox:
            align (.5, 1.0)

            imagebutton:
                at xyzoom(.9)
                auto "back_%s"
                margin (5, 3)
                action Rollback()
            imagebutton:
                at xyzoom(.9)
                auto "save_%s"
                margin (5, 3)
                action ShowMenu("save", enter="game")
            imagebutton:
                at xyzoom(.9)
                auto "load_%s"
                margin (5, 3)
                action ShowMenu("load", enter="game")
            imagebutton:
                at xyzoom(.9)
                auto "menu_%s"
                margin (5, 3)
                action ShowMenu()


style city_frame:
    align (.5, .45)
    xysize (700, 300)
    padding (20, 20)

style city_frame:
    variant "touch"
    align (.5, .25)
    xysize (740, 400)

style city_grid:
    variant "touch"
    spacing 15

style city_button:
    is default
    background None

style city_button_text:
    is default
    font "fonts/blogger.ttf"
    outlines [(2, "#000")]
    size 21
    kerning .2
    line_leading 2
    line_spacing 2
    idle_color "#fff"
    hover_color "#00f"
    insensitive_color "#80808060"
    insensitive_outlines [(2, "#00000080")]
